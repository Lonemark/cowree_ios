//
//  Header.h

//
//  Created by Rahul Sharma on 04/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#ifndef Header_h
#define Header_h

#define  mAdsCampaignKey            @"adsCampaignKey"
#define  COUNTRY_NAME @"name"
#define  COUNTRY_CODE @"countryCode"
#define  CURRENCY_CODE @"currencyCode"
#define  CURRENCY_SYMBOL @"currencytSymbol"
#define ACCEPTABLE_CHARACTERS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_."

#define mKeyForSavingCategoryList               @"arrayOfcategoryListFromAPI"
#define mFacebookTitle                          @"Facebook"
#define mContactsTitle                          @"Contacts"
#define mGuestToken                             @"guestUser"
#define mLimitValue                             @"20"
#define mPagingValue                            40

#define mBaseColor                              [UIColor colorWithRed:211/255.0f green:35/255.0f blue:35/255.0f alpha:1.0f]

#define mBaseColor2                             [UIColor colorWithRed:211/255.0f green:35/255.0f blue:35/255.0f alpha:1.0f]

#define mTextBaseColor                          [UIColor colorWithRed:40/255.0f green:40/255.0f blue:40/255.0f alpha:1.0f]

#define mDividerColor                          [UIColor colorWithRed:219/255.0f green:219/255.0f blue:219/255.0f alpha:1.0f]


#define mPresentAlertController                 [self presentViewController:controller animated:YES completion:nil]

#define mCloseButtonImageName                   @"filter_close_button"
/**
 String Constants for Navigation Bar
 */
#define mNavigationBarColor               [UIColor colorWithRed:250/255.0f green:250/255.0f blue:250/255.0f alpha:1.0f]

#define mSearchCollectioncellIdentifier    @"searchcollectionCellIndentifier"

#define mNavigationBackButtonImageName      @"navigationBackButton"
#define mReport_Icon                        @"item_Report"
#define mCloseButton                        @"close_button"
#define mAddPeopleIconOFF                   @"profile_add_user_icon_off"
#define mAddPeopleIconON                    @"addUser_purple"
#define mEditProfileOFF                     @"editProfile_Icon"
#define mEditProfileON                      @"edit_purple"
#define mSettingsON                         @"settings_purple"
#define mSettingsOFF                        @"profile_setting_button"



/*------------------------------------------------*/
#pragma mark
#pragma mark - StoryBoard IDs
/*-------------------------------------------------*/

#define mSplashScreenID                     @"SplashScreenStoryboardID"
#define mLoginScreenID                      @"loginVcStoryBoardId"
#define mSignUpScreenStoryboardID           @"SignupScreen"
#define mForgotPasswordStoryboardID         @"ForgotPasswordStoryboardID"
#define mPostScreenStoryboardID             @"PostListingsStoryboard"
#define mPermissionStoryboardID             @"askingPermissionVcStoryBoardId"
#define mCountyListStoryboardID             @"CountryCodeStoryboardID"
#define mCurrencyStoryboardID               @"CurrencySelectVC"
#define mSearchPostsID                      @"SearchPostsStoryboardID"
#define mWebviewStoryboardID                @"LinkWebViewController"
#define mOtpStoryboardID                    @"OTPStoryboardID"
/*------------------------------------------------*/
#pragma mark
#pragma mark - Notifications Name
/*-------------------------------------------------*/
#define mDeletePostNotifiName                 @"DeletePost"
#define mfavoritePostNotifiName              @"addfavoritePostNotification"
#define mSellingPostNotifiName               @"addSellingPostNotification"
#define mLibraryNotification                 @"addAgainFromLibrary"
#define mSellingToSoldNotification           @"changeSellingToSold"
#define mSellingAgainNotification            @"sellItAgain"
#define mUpdateNumberNotification            @"passPhoneData"
#define mUpdateEmailNotification             @"passEmailData"
#define mUpdatePostDataNotification          @"updatePostData"
#define mAddNewPost                          @"addNewPost"
#define mShowAdsCampaign                     @"showAdsCampaignNotification"
#define mTriggerCampaign                     @"triggerAdsCampaign"
#define mSavePayPalNotify                    @"savePayPalNotification"
#define mUpdatePromotedPost                  @"postIsPromoted"
/*------------------------------------------------*/
#pragma mark
#pragma mark - loginScreen string constant
/*-------------------------------------------------*/

#define mLoginButtonBorderColor                 [UIColor colorWithRed:203/255.0f green:228/255.0f blue:251/255.0f alpha:1.0f]
#define mLoginButtonEnableBackgroundColor       [UIColor colorWithRed:204/255.0f green:0/255.0f blue:0/255.0f alpha:1.0f]
#define mLoginButtonDisableBackgroundColor       [UIColor colorWithRed:204/255.0f green:0/255.0f blue:0/255.0f alpha:0.7f]
#define mSignupButtonDisableBackgroundColor       [UIColor colorWithRed:204/255.0f green:0/255.0f blue:0/255.0f alpha:0.5f]
#define mLoginTextColor                         [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.6f]
#define mSignupTextColor                        [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.4f]
#define mUserNormalLogin                        @"1"
#define mUserFacebookLogin                      @"2"
#define mUserGooglePlusLogin                    @"3"
//API Constants


/*------------------------------------------------*/
#pragma mark
#pragma mark - SignupScreen string constant
/*-------------------------------------------------*/
#define mSignupFirstTime                         @"signupFirstTimeShowWelcome"
#define mUserNormalSignup                        @"1"
#define mUserFacebookSignup                      @"2"
#define mUserGooglePlusSignup                   @"3"

#define mContentHeightConstraint                @"200"
#define navLeftNormalImage                      @"signup_back_button"
#define navLeftSelectedImage                    @"signup_back_button"
#define mCountryCode                            @"+91"
#define mSignupType                             @"2"
#define mImageForCheckAvailability              @"signup_rightusername_icon"
#define mImageForNotAvail                       @"signup_wrongusername_icon"

/*------------------------------------------------*/
#pragma mark
#pragma mark - HomeScreen string constant
/*-------------------------------------------------*/
#define mProductFiltersCellId                   @"cellForFilteredItems"
#define mTopTableViewCellID                     @"topTableViewCell"
#define mPeopleTableViewCellID                  @"peopleTableViewCell"



//*------------------------------------------------*/
#pragma mark
#pragma mark - ExploreScreen string constant
/*-------------------------------------------------*/


#define mAddMultiImagesCellID                   @"addImagesCell"
#define mWidthOfAddMultiImagesCell              80
#define mHeightOfAddMultiImagesCell             80
#define mPlaceSuggestionCellID                  @"cellIdentifier"

#define mArrayOfCurrency                         @[@"USD",@"INR"]

//*------------------------------------------------*/
#pragma mark
#pragma mark -Product Details string constant
/*-------------------------------------------------*/

#define mTableViewCellSepratorColor              [UIColor colorWithRed:225/255.0f green:225/255.0f blue:225/255.0f alpha:1.0f]


//*------------------------------------------------*/
#pragma mark
#pragma mark - Pin Address Screen Constants
/*-------------------------------------------------*/

#define mPinAddressStoryboardID                    @"PinAddress"


//*------------------------------------------------*/
#pragma mark
#pragma mark -Settings string constant
/*-------------------------------------------------*/
#define mArrayForSectionHeaders                @[@"Follow People",@"Account",@"Payment",@"Support",@"About",@""]
#define mImageArray                            @[@"settings_facebook_icon",@"settings_contacts_icon"]
#define mTitleArryForSecondSection             @[@"Edit Profile"]
#define mTittleArrayForFourthSection           @[@"Privacy Policy",@"Terms & Conditions"]
#define mTittleArrayForFifthSection            @[@"Clear Search History",@"Add Account",@"Log Out"]


//*------------------------------------------------*/
#pragma mark
#pragma mark -StoryboardId Identifiers
/*-------------------------------------------------*/


#define mInstaTableVcStoryBoardId       @"instaTableViewController"
#define mDiscoverPeopleVcSI             @"discoverPeopleStoryBoardId"
#define numberOfFbFriendFoundInPicogram @"numberOfYourFbFriendFoundInPicogram"
#define numberOfContactsFoundInPicogram @"numberOfContactsFoundInPicogram"
#define mpaypalView                     @"paypalViewController"

#define mLinkPrivacyWebViewController              @"LinkPrivacyWebViewController"
#define mConnectToFaceBookFriendsStoryBoardId      @"connectToFaceBookFriendsStoryBoardId"
#define mReportVcStoryBoardId                      @"feedBackVcStoryBoardId"
#define mDetailWebViewStoryBoardId                 @"detailWebViewStoryBoardId"


//*------------------------------------------------*/
#pragma mark
#pragma mark - Edit Product ScreenConstant
/*-------------------------------------------------*/
#define mEditItemStoryBoardID                       @"EditProductStoryboard"

//*------------------------------------------------*/
#pragma mark
#pragma mark -Reusable Cell ID
/*-------------------------------------------------*/

#define mCollectionViewCellIDInNewsFeed           @"LikersCellInNewsFeed"
#define mCollectionViewCellIDInProductDetails     @"LikersProfileCellInProductDetails"


// Chat Suggesstion Array

#define mChatSuggestionsArray            @[@"Is it available?",@"I'm interested in buying.",@"Request Phone no.",@"Is price negotiable?", @"Fix up meeting place."]



//*------------------------------------------------*/
#pragma mark
#pragma mark - Promotions Screen
/*-------------------------------------------------*/
// In App Product Ids

#define INAPP_KEY_100_Clicks                         @"com.yelo.100Clicks"
#define INAPP_KEY_200_Clicks                         @"com.yelo.200Clicks"
#define INAPP_KEY_300_Clicks                         @"com.yelo.300Clicks"
#define INAPP_KEY_500_Clicks                         @"com.yelo.500Clicks"

#endif /* Header_h */
