//
//  WebServiceConstants.h
//
//
//  Created by 3Embed on 03/12/15.
//  Copyright (c) 2014 3Embed. All rights reserved.
//


// WEBSERVICE URLs
#define iPhoneBaseURL     @"https://api.cowree.com/"
#define mChatAPIBaseUrl    @"https://mqtt.cowree.com/"
#define mImageUrlForChat   @"http://cowree.com/chat/profilePics"
#define mMediaUploadForChat @"https://upload.cowree.com/"

// Links

#define mIosBundleId            @"com.cowree.dev"
#define mDomainForDeepLinking   @"cowree.page.link"
#define mAndroidBundleId        @"com.cowree.com"
#define SHARE_LINK       @"https://cowree.com/item/"
#define adminGalleryURL  @""
#define TERMS_CONDITIONS_LINK  @"https://cowree.com/terms"
#define PRIVACY_LINK     @"https://cowree.com/privacy"
#define mReportEmail      @"support@cowree.com"
//
// MQTT HOST & PORT


#define mMqttUsername    @"cowree"
#define mMqttPassword    @"Lcww56dk2m4T9M255S"
#define mMQTTHost    @"46.101.171.206"
#define mPort        @"1883"

//


// AUTH Username / Passwords for SSl

#define authUsername  @"basicAuth"
#define authPassword   @"&jno-@8az=wSo*NHYVGpF^AQ?4yn36ZvW5ToUCUN+XGOuC?sz#SE$oxXVbwQGP|3WFyjcTAj2SIRQnLE|vo^-|-ATV5FZUf2*5A3Oiu|_EOMmG==&iApzQL3R7HHQj?jtb0mc2mT$Y%Isrgrxveld#Z^g3-ul^|0xAITganIuF23J0waSa6z6aP_+%De5LqtuY&ptx?qhZySECdyE^*4R^b*hFjQ-9?cCSJNfROzztEYbRyN=SqDyhhpzSmmP|Eb"

#define mAuthorization @"KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj"


//
//Facebook Audience Ad Network

#define mFacebookNativeAdId  @"150504785543639_1170652729744479"



// GOOGLE CONSOLE KEYS
#define mFCMLegacyKey        @"key=AIzaSyC9S35e95kNIBLKDvxmG-qD-bA-9wat_ck"
#define mGoogleAPIKey       @"AIzaSyCm7KSuYLPig4TWvkCK26-2SHMItb0MO8g"
#define mGoogleAdsAppId         @"ca-app-pub-5991865647998806~5111193168"
#define mGoogleInterstitialAdsUnitId   @"ca-app-pub-5991865647998806/8667294792"
#define mGoogleBannerAdsUnitId  @"ca-app-pub-5991865647998806/1503120645"
//


//  ENUMS FOR REQUESTS
typedef enum : NSUInteger {
    //Authentication Type
    RequestTypeLogin,
    RequestTypePhoneNumberCheck,
    RequestTypeUserNameCheck,
    RequestTypeEmailCheck,
    RequestTypenewRegister,
    RequestTypeotpGeneration,
    RequestTypeEditNumberOTP,
    RequestTypemakeresetPassword,
    RequestToLogUserDevice,
    RequestTypeLogout,
    
    // SYNC
    RequestTypePhoneContactSync,
    RequestTypeLoginfaceBookContactSync,
  

    // Post Listing
    RequestTypePost,
    
    // Follow/UnFollow
    RequestTypeFollow,
    RequestTypeGetFollowersList,
    RequestTypeGetFollowingList,
    RequestTypeGetMemberFollowersList,
    RequestTypeGetMemberFollowingList,
    
    // Home
     RequestTypeGetExploreposts,
    
    // Filter
    RequestTypeSearchProductsByFilters,
    
    //Search Post/People
    RequestTypeGetSearchPeople,
    RequestTypeGetSearchForPosts,
    
    // Cloudinary Signature
    RequestTypeCloudinaryCredintials,
    
    // Social
    RequestTypegetPostsInHOmeScreen,
    
    // Review/Comments
    RequestTypeGetCommentsOnPost,
    RequestTypePostComment,
    RequestTypedeleteComments,
    
    // Profile/Posts
    RequestTypeGetPosts,
    RequestTypeUserExchangedPosts,
    RequestTypemakeUserProfileDetails,
    RequestTypeProfileDetails,
    RequestTypePostsLikedByUser,
    RequestToVerifyWithFacebook,
    RequestToVerifyWithGooglePlus,
    
    // EDIT PROFILE
    RequestTypeEditProfile,
    RequestTypeSavingProfile,
    RequestTypeEmailCheckInEditProfile,
    RequestTypePhoneNumberCheckEditProfile,
    
    // EDIT/DELETE
    RequestTypeDeletePost,
    RequestTypeEditPost,

    RequestTypeupdatePhoneNumber,
    RequestTypeupdateEmail,
    // ACTIVITY/ NOTIFICATIONS
    RequestTypefollowingActivity,
    RequestTypeOwnActivity,
    RequestTypeunseenNotificationCount,
    
    // Like/Unlike
    RequestTypeGetAllLikesOnPost,
    RequestTypeLikeAPost,
    RequestTypeUnlikeAPost,
    
    
    // DISCOVER PEOPLE
    RequestTypeDiscoverPeople,
    RequestTypehideFromDiscovery,
    
    
    // REPORT
    RequestTypeGetReportReasonForPost,
    RequestTypereportPost,
    
    // Post Details
    RequestTypeGetPostDetails,
    
    // Get Category
    RequestTypeGetCategories,
    RequestTypeSubCategory,
    
    // Product Details
    RequestTypeGetPostsByUsers,
    RequestTypeGetPostsForGuests,
    
    
    // SOLD
    RequestTypemarkSold,
    RequestTypesoldElseWhere,
    
    // Rate User
    RequestTyperateForSeller,
    
    // SELLING AGAIN
    RequestToMarkAsSelling,
    
    // Accepted offers
    RequestTypeacceptedOffers,
    
    

    // Ads Campaign
    RequestUserCampaign,
    RequestGetCampaign,
    
    // Save Pypal
    RequestToSavePaypal,

    
    // InSights
    RequestToGetInsights,
    RequestToGetMonthInsights,
    RequestToGetCityInsights,
    
    // InApp Purchase
    RequestForPurchasePlans,
    
    // Check Session Status
    RequestTypeSession,
    
    // SWAP SUGGESTIONS
    RequestPostSuggesstion,
    
    // Get Chat Message Suggestion
    RequestTypeChatMessageSuggestion
} RequestType;



//
// API's
//

// AUTHENTICATION

#define mRequestTypeLogin                                   @"login"
#define mLogUserDevice                                      @"logDevice"
#define mLogGuestUserDevice                                 @"logGuest"
#define mRequestTypeEmailCheck                              @"emailCheck"
#define mRequestTypePhoneNumberCheck                       @"phoneNumberCheck"
#define mRequestTypeUserNameCheck                           @"usernameCheck"
#define mRequestTypenewRegister                             @"register"
#define mRequestTypeotpGeneration                           @"otp"
#define mRequestTypeEditNumberOTP                           @"profile/phoneNumber"
#define mRequestTypresetPassword                            @"resetPassword"

// UNSEEN NOTIFICATION
#define mRequestTypeunseenNotificationCount                 @"unseenNotificationCount"

// SOLD
#define mRequestTypesoldElseWhere                           @"sold/elseWhere"
#define mRequestTypemarkSold                                @"markSold"
#define mSoldSomewhereElse                                  @"sold/elseWhere"

// ACCEPTED OFFERS
#define mRequestTypeacceptedOffers                          @"acceptedOffers"


// SYNC
#define mRequestTypeLoginfaceBookContactSync                @"facebookContactSync"
#define mRequestTypePhoneContactSync                        @"phoneContacts"

// FOLLOW/UNFOLLOW
#define mRequestTypeFollow                                  @"follow/"
#define mRequestTypeUnFollow                                @"unfollow/"

// POST BY ID
#define mGetPostsByusers                                    @"getPostsById/users"
#define mGetPostsForGuests                                  @"getPostsById/guests"
#define mGetPostDetails                                     @"getPostsById"

// HOME / EXPLORE
#define mgetPostsForUsers                                 @"allPosts/users/m"
#define mgetPostsForGuests                                @"allPosts/guests/m"

// POST LISTING
#define mBusinessPostRequest                                @"product/v2"

// CATEGORY
#define mGetCategories                                      @"getCategories"
#define mSubCategory                                     @"subCategory"

// DISCOVE PEOPLE
#define mRequestTypehideFromDiscovery                    @"hideFromDiscovery"
#define mdiscoverPeople                            @"discover-people-website"

// FILTER PRODUCTS
#define mFilterProducts                                     @"filterProduct"

// EDIT PRODUCT
#define mEditPostedProduct                                  @"product/v2"
#define mdeletePost                                         @"product/v2"

// PROFILE
#define mUserProfile                                        @"profile"
#define mMemberProfile                                      @"profile/users"
#define mMemberProfileForGuest                              @"profile/guests"
#define mUserPosts                                          @"profile/posts"

#define mExchangedPosts   @"myExchanges"
#define mMemberSellingPosts                                 @"profile/posts/"
#define mMemberPostsForGuest                           @"profile/guests/posts"
#define mMembergetUserProfileBasics                    @"getUserProfileBasics"
#define mPostsLikedByUser                                   @"likedPosts"

// SEARCH
#define mgetSearchForPosts                                  @"search/"

// FOLLOWERS/FOLLOWINGS
#define mgetFollowersList                                   @"getFollowers"
#define mgetFollwingList                                    @"getFollowing"
#define mgetMemberFollowersList                          @"getMemberFollowers"
#define mgetMemberFollowingList                          @"getMemberFollowing"

// CLOUDINARY CREDS
#define mgetCloudinaryCredintials                           @"getSignature"

// SOCIAL/NEWSFEED
#define mgetPostsInHOmeScreen                               @"home"

// COMMENTS/ REVIEWS
#define mPostComment                                        @"comments"
#define mGetCommentsOnPost                            @"getPostComments"
#define mRequestTypedeleteComments               @"deleteCommentsFromPost"

// EDIT PROFILE
#define mRequestTypeSavingProfile                           @"saveProfile"
#define mRequestTypeEditProfile                             @"editProfile"
#define mRequestTypePhoneNumberUpdate                    @"profile/phoneNumber"
#define mRequestTypePhoneSendEmail                      @"email/me"


// SELLING AGAIN
#define mMarkAsSelling                                      @"markSelling"


// LIKE / UNLIKE
#define mLikeAPost                              @"like"
#define mUnlikeAPost                            @"unlike"
#define mGetAllLikesOnPost                      @"getAllLikes"

// RATE USER
#define mRequestTyperateForSeller               @"rate/"



#define payPalLink                             @"PaypalData"
#define mPayPalMe                               @"paypal/me"

// ADS CAMPAIGN
#define mUserCampaign                           @"user/campaign"
#define mRuncampaign                            @"user/runcampaign"

// INSIGHTS
#define mInsights                               @"insights"

// IN-APP PURCHASE
#define mPurchasePromoPlan                      @"inAppPurchase"

// VERIFY
#define mVerifyWithFacebook                     @"facebook/me"
#define mVerifyWithGoogle                       @"google/me"


// REPORT
#define mReportPostReason                       @"postReportReason"
#define mReportUserReason                       @"reportReason"
#define mRequestTypereportPost                  @"reportPost"
#define mReportUser                             @"report/"


// ACTIVITY/ NOTIFICATIONS
#define mRequestTypefollowingActivity           @"followingActivity"
#define mRequestTypeOwnActivity                 @"selfActivity"


// Exchange
#define mPostSuggession                         @"suggestionPost"

// ChatMessage Suggestion
#define mChatMessagesSuggestion                 @"message"



#pragma mark - PARAMETERS-
//
// PARAMETERS
//

#define mqttID                                   @"mqttId"
#define mLoginType                               @"loginType"
#define mEmail                                   @"email"
#define mphoneNumber                             @"phoneNumber"
#define mfbuniqueid                              @"facebookId"
#define mUserName                                @"username"
#define mUserId                                  @"userId"
#define mMqttId                                  @"mqttId"
#define mPswd                                    @"password"
#define mDeviceType                              @"deviceType"
#define mDeviceId                                @"deviceId"
#define mProfileUrl                              @"profilePicUrl"
#define mSignUpType                              @"signupType"
#define cloudinartyDetails                       @"cloudinaryDetails"
#define misPrivate                               @"isPrivate"
#define mdeviceToken                             @"deviceToken"
#define mGooglePlusAccessToken                   @"googleToken"
#define mCampaignId                              @"campaignId"
#define userDetailkeyWhileRegistration       @"userDetailWhileRegistration"
#define mpushToken                           @"pushToken"
#define mfaceBookId                          @"facebookId"
#define mauthToken                           @"token"
#define mcontacts                            @"contactNumbers"
#define mtype                                @"type"
#define mmailUrl                             @"mainUrl"
#define mthumbeNailUrl                       @"thumbnailUrl"
#define mpostCaption                         @"postCaption"
#define mhashTags                            @"hashTags"
#define mplace                               @"place"
#define musersTagged                         @"usersTagged"
#define muserNameTofollow                    @"userNameToFollow"
#define muserNameToUnFollow                  @"unfollowUserName"
#define mGooglePlusId                        @"googleId"
#define mPayPalUrl                           @"paypalUrl"
#define mfaceBookId                          @"facebookId"
#define mFacebookAccessToken                 @"accessToken"
#define macesstoken                          @"xaccesstoken"
#define muserTosearch                        @"userToBeSearched"
#define msearchPeopleForGuest                @"guests/search/member"
#define mKeyToSearch                         @"keyToSearch"
#define mcomment                             @"comment"
#define mposttype                            @"postType"
#define mpostid                              @"postId"
#define mPromoPlanId                         @"planId"
#define mmemberName                          @"membername"
#define mMember                              @"member"
#define mDescription                         @"description"
#define mlocation                            @"location"
#define mlatitude                            @"latitude"
#define mlongitude                           @"longitude"
#define mPushTokenKey                        @"pushToken"
#define mfullName                            @"fullName"
#define mwebsite                             @"website"
#define mbio                                 @"bio"
#define mgender                              @"gender"
#define mlimit                               @"limit"
#define motp                                 @"otp"
#define mContainerHeight                     @"containerHeight"
#define mcontainerWidth                      @"containerWidth"
#define maddToSearchKey                      @"searchKey"
#define mcommentId                           @"commentId"
#define mmembername                          @"membername"
#define mhasAudio                            @"hasAudio"
#define mfeature                             @"feature"
#define mproblemExplaination                 @"problemExplaination"
#define mlogout                              @"logout/m"
#define mLimit                               @"limit"
#define moffset                              @"offset"
#define mSortby                              @"sortBy"
#define mdistanceOrder                       @"distanceOrder"
#define mPostedWithIn                        @"postedWithin"
#define mMinPrice                            @"minPrice"
#define mMaxPrice                            @"maxPrice"
#define mDistance                            @"distance"
#define mCurrLatt                            @"currentLatitude"
#define mCurrLongi                           @"currentLongitude"
#define mCountryShortName                    @"countrySname"
#define mCity                                @"city"
#define mReasonId                            @"reasonId"
#define mReportedUser                        @"reportedUser"
#define mDurationType                        @"durationType"
#define mPhotoType                                        @"0"
#define mProductName                                      @"productName"
#define mMainImgeUrl                                    @"mainUrl"
#define mMainImgeThumb                                  @"thumbnailImageUrl"
#define mMainImgeHeight                                  @"containerHeight"
#define mMainImgeWidth                                  @"containerWidth"
#define mImgHeight1                                      @"containerHeight1"
#define mImgHeight2                                      @"containerHeight2"
#define mImgHeight3                                      @"containerHeight3"
#define mImgHeight4                                      @"containerHeight4"
#define mImgWidth1                                       @"containerWidth1"
#define mImgWidth2                                       @"containerWidth2"
#define mImgWidth3                                       @"containerWidth3"
#define mImgWidth4                                       @"containerWidth4"
#define mImgUrl1                                         @"imageUrl1"
#define mImgUrl2                                         @"imageUrl2"
#define mImgUrl3                                         @"imageUrl3"
#define mImgUrl4                                         @"imageUrl4"
#define mThumbUrl1                                       @"thumbnailUrl1"
#define mThumbUrl2                                       @"thumbnailUrl2"
#define mThumbUrl3                                       @"thumbnailUrl3"
#define mThumbUrl4                                       @"thumbnailUrl4"
#define mSearchCategory                                  @"searchKey"
#define mCategory                                        @"category"
#define mcategoryId                                     @"categoryId"
#define mcategoryNodeId                                 @"categoryNodeId"
#define msubCategoryNodeId                              @"subCategoryNodeId"
#define mLanguageCode                                    @"languageCode"
#define mSubCategory                                     @"subCategory"
#define mCondition                                       @"condition"
#define mCurrency                                        @"currency"
#define mPrice                                           @"price"
#define mImageCount                                      @"imageCount"
#define mFirmOnPrice                                     @"negotiable"
#define mTaggedProductCoordinates                        @"tagProductCoordinates"
#define mTagProductStrings                               @"tagProduct"
#define mcloudinaryPublicId                              @"cloudinaryPublicId"
#define mcloudinaryPublicId1                             @"cloudinaryPublicId1"
#define mcloudinaryPublicId2                             @"cloudinaryPublicId2"
#define mcloudinaryPublicId3                             @"cloudinaryPublicId3"
#define mcloudinaryPublicId4                             @"cloudinaryPublicId4"
#define mLabel                              @"label"

#define favDBdocumentID         @"favDBdocumentID"
#define contacDBDocumentID      @"contacDBDocumentID"
#define kController                         @"ControllerType"

