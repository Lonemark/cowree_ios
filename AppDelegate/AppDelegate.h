//
//  AppDelegate.h

//
//  Created by Rahul Sharma on 09/02/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <Google/SignIn.h>
#import <UIKit/UIKit.h>
#import <FBSDKLoginKit/FBSDKLoginManagerLoginResult.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginButton.h>
//#import <CoreData/CoreData.h>
#import <UserNotifications/UserNotifications.h>
@import GoogleMobileAds;

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate,GADInterstitialDelegate>

@property (strong, nonatomic) UIWindow *window;

@property(nonatomic, strong) GADInterstitial *interstitial;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) UIView *internetConnectionErrorView;
@property(nonatomic,assign)Float32 lat;
@property(nonatomic,assign)Float32 log;





@end

