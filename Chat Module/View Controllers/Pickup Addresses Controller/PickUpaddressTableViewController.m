//
//  PickUpaddressTableViewController.m
//
//  Created by Rahul Sharma on 3/15/16.
//  Copyright © 2016 3embed. All rights reserved.
//

#import "PickUpaddressTableViewController.h"
#import "PickupTableViewCell.h"
#import "Helper.h"

@interface PickUpaddressTableViewController ()<UISearchBarDelegate>

@end

NSString *const apiKey = @"AIzaSyBgP0zBlOZIs2_-5ZKUadAAdR6-m_WCMGM";

@implementation PickUpaddressTableViewController
@synthesize latitude,longitude;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.navigationItem.title = @"Search Location" ;
    

    latitude = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"]];
    longitude = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"logitute"]];
    _searchBar.delegate = self;
    
    _mAddress = [NSMutableArray new];
    
}
-(void)viewDidAppear:(BOOL)animated{
    
    [_searchBar becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _mAddress.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PickupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pickupCell"];
    
    NSDictionary *searchResult = [_mAddress objectAtIndex:indexPath.row];
    cell.mainLbl.text = [searchResult[@"terms"] objectAtIndex:0][@"value"];
    cell.subLbl.text = searchResult [@"description"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *searchResult = [self.mAddress objectAtIndex:indexPath.row];
    NSString *placeID = [searchResult objectForKey:@"reference"];

    [self retrieveJSONDetailsAbout:placeID withCompletion:^(NSArray *place)
     {

         NSString *add1 = [NSString stringWithFormat:@"%@", flStrForObj([place valueForKey:@"name"])];
         NSString *add2 = [NSString stringWithFormat:@"%@",flStrForObj([place valueForKey:@"formatted_address"])];
         if (add1.length == 0)
         {
             add1 = [add1 stringByAppendingString:flStrForObj([place valueForKey:@"formatted_address"])];
             add2 = @"";
         }
         
         NSString *late = [NSString stringWithFormat:@"%@,",[place valueForKey:@"geometry"][@"location"][@"lat"]];
         NSString *longi = [NSString stringWithFormat:@"%@",[place valueForKey:@"geometry"][@"location"][@"lng"]];
         
         NSString *latlog =[late stringByAppendingString:longi];
         NSDictionary *dict = @{
                                @"name":add1,
                                @"address":add2,
                                @"latlog":latlog
                                };
         if (_delegate && [_delegate respondsToSelector:@selector(addressSelectedWithData:)]) {
             [_delegate addressSelectedWithData:dict];
         }
         [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"serachbuttoncliked"];
         [self dismissViewControllerAnimated:NO completion:nil];
     }];
}

#pragma serach delegates

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [self.autoCompleteTimer invalidate];
    [self searchAutocompleteLocationsWithSubstring:self.substring];
    [self.searchBar resignFirstResponder];
    
   // [self.tableView reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{


    NSString *searchWordProtection = [self.searchBar.text stringByReplacingOccurrencesOfString:@" " withString:@""];
  //  NSLog(@"Length: %lu",(unsigned long)searchWordProtection.length);
    
    if (searchWordProtection.length != 0) {
        
        [self runScript];
        
    } else {
       // NSLog(@"The searcTextField is empty.");
    }
}

-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{

    self.substring = [NSString stringWithString:self.searchBar.text];
    self.substring= [self.substring stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    self.substring = [self.substring stringByReplacingCharactersInRange:range withString:text];

    if ([self.substring hasPrefix:@"+"] && self.substring.length >1) {
        self.substring  = [self.substring substringFromIndex:1];
       // NSLog(@"This string: %@ had a space at the begining.",self.substring);
    }
    return YES;
}


-(void)runScript{
    [self.autoCompleteTimer invalidate];
    self.autoCompleteTimer = [NSTimer scheduledTimerWithTimeInterval:0.65f target:self selector:@selector(searchAutocompleteLocationsWithSubstring:) userInfo:nil repeats:NO];
}

- (void)searchAutocompleteLocationsWithSubstring:(NSString *)substring
{
    [self.mAddress removeAllObjects];
    [self.tableView reloadData];
    
    if (![_pastSearchWords containsObject:_substring]) {
        [_pastSearchWords addObject:_substring];
       // NSLog(@"Search: %lu",(unsigned long)self.pastSearchResults.count);
        
        [self retrieveGooglePlaceInformation:self.substring withCompletion:^(NSArray * results)
         {
            // NSLog(@"Auot_search =%@",results);
             [self.mAddress addObjectsFromArray:results];
             
             _isSearchResultCome = YES;
             
             NSDictionary *searchResult = @{@"keyword":self.substring,@"results":results};
             [self.pastSearchResults addObject:searchResult];
             [self.tableView reloadData];
             if (_delegate && [_delegate respondsToSelector:@selector(arrayOFaddress:)]) {
                 [_delegate arrayOFaddress:self.mAddress];
             }
             
             
         }];

        
    }
    else{
        
        for (NSDictionary *dict in  self.pastSearchResults) {
            if ([[dict objectForKey:@"keyword"] isEqualToString:self.substring]) {
                [self.mAddress addObjectsFromArray:[dict objectForKey:@"results"]];
                [self.tableView reloadData];
            }
            
        }
        
        
        
    }
    
    
}

#pragma mark - Google API Requests
-(void)retrieveGooglePlaceInformation:(NSString *)searchWord withCompletion:(void (^)(NSArray *))complete{
 
     NSString *searchWordProtection = [searchWord stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (searchWordProtection.length != 0) {
        
        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=establishment|geocode&location=%@,%@&radius=500po &language=en&key=%@",searchWord,latitude,longitude,apiKey];
      //  NSLog(@"AutoComplete URL: %@",urlString);
        
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration:defaultConfigObject delegate:nil delegateQueue:[NSOperationQueue mainQueue]];
        
        NSURLRequest *request  = [NSURLRequest requestWithURL:url];
        
        NSURLSessionDataTask *task  =[delegateFreeSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
            
            NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            NSArray *results = [jSONresult valueForKey:@"predictions"];

            
            
            
            if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]){
                if (!error){
                    NSDictionary *userInfo = @{@"error":jSONresult[@"status"]};
                    NSError *newError = [NSError errorWithDomain:@"API Error" code:666 userInfo:userInfo];
                    complete(@[@"API Error", newError]);
                    return;
                }
                complete(@[@"Actual Error", error]);
                return;
            }else{
                complete(results);
            }

            
            
        }];
        
       [task resume];
    
    }
    
    
    
}
     
     
     
-(void)retrieveJSONDetailsAbout:(NSString *)place withCompletion:(void (^)(NSArray *))complete {
         NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?reference=%@&key=%@",place,apiKey];
         NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
         NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
         NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
         NSURLRequest *request = [NSURLRequest requestWithURL:url];
         NSURLSessionDataTask *task = [delegateFreeSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
             NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
             NSArray *results = [jSONresult valueForKey:@"result"];
             
             if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]){
                 if (!error){
                     NSDictionary *userInfo = @{@"error":jSONresult[@"status"]};
                     NSError *newError = [NSError errorWithDomain:@"API Error" code:666 userInfo:userInfo];
                     complete(@[@"API Error", newError]);
                     return;
                 }
                 complete(@[@"Actual Error", error]);
                 return;
             }else{
                 complete(results);
             }
         }];
         [task resume];
     }


@end
