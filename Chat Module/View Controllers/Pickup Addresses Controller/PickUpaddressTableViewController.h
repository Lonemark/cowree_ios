//
//  PickUpaddressTableViewController.h
//
//  Created by Rahul Sharma on 3/15/16.
//  Copyright © 2016 3embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol gotAddress <NSObject>

-(void)arrayOFaddress:(NSMutableArray *)places;
-(void)addressSelectedWithData:(NSDictionary *)data;

@end

@interface PickUpaddressTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property NSTimer *autoCompleteTimer;
@property NSString *substring;
@property NSMutableArray *pastSearchWords;
@property NSMutableArray *pastSearchResults;
@property(nonatomic,assign) BOOL isSearchResultCome;
@property (strong, nonatomic) NSMutableArray *mAddress;

@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;
@property (assign, nonatomic) NSInteger locationType;

@property (weak) id <gotAddress> delegate;



@end
