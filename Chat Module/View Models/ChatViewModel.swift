//
//  ChatViewModel.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 01/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit


class ChatViewModel: NSObject {
    
    var chat : Chat
    let couchbaseObj = Couchbase.sharedInstance
    let api = API()
    
    typealias completionBlockType = ([String : Any]?) ->Void
    
    init(withChatData chat: Chat) {
        self.chat = chat
    }
    
    var messageArray : [String]? {
        return self.chat.messageArray
    }
    
    var initiated : Bool? {
        return self.chat.initiated
    }
    
    var hasNewMessage : Bool {
        guard let hasNewMessage = self.chat.hasNewMessage else { return false }
        return hasNewMessage
    }
    
    var newMessage : String? {
        return self.chat.newMessage
    }
    
    var newMessageTime : String? {
        return self.chat.newMessageTime
    }
    
    var newMessageDateInString : String? {
        return self.chat.newMessageDateInString
    }
    
    var lastMsgTimeInHours : String? {
        
        guard let date = self.chat.msgDate else { return ""}
        let hours = DateExtension().lastMessageInHours(date: date)
        return hours
    }
    
    var newMessageCount : String? {
        return self.chat.newMessageCount
    }
    
    var lastMessageDate :String? {
        return self.chat.lastMessageDate
    }
    
    var recieverUIDArray : [String]? {
        return self.chat.recieverUIDArray
    }
    
    var recieverDocIDArray : [String]? {
        return self.chat.recieverDocIDArray
    }
    
    var name : String? {
        return self.chat.name
    }
    
    var offerType : String? {
        return self.chat.offerType
    }
    
    var imageURL : URL? {
        guard let imageURL = self.chat.image else { return nil }
        return URL(string :imageURL)
    }
    
    var secretID : String? {
        return self.chat.secretID
    }
    
    var userID : String? {
        return self.chat.userID
    }
    
    /// Current receiver ID for chat.
    var recieverID : String? {
        guard let recipientID = self.chat.recipientId else {
            return self.chat.userID
        }
        if recipientID.count == 0 {
            return self.chat.userID
        }
        else if recipientID == Helper.getMQTTID() {
            return self.chat.userID
        }
        return recipientID
    }
    
    var senderID : String?  {
        return self.chat.senderId
    }
    
    /// if Product is sold then it wil return true else false.
    var isProductSold : Bool {
        return self.chat.isProductSold
    }
    
    var docID : String? {
        return self.chat.docID
    }
    
    var userImageURL : URL? {
        guard let imagURL = self.chat.profilePicUrl else { return nil }
        return URL(string :imagURL)
    }
    
    var chatID : String? {
        return self.chat.chatID
    }
    
    var wasInvited : Bool {
        guard let wasInvited = self.chat.wasInvited else { return false }
        return wasInvited
    }
    
    var destructionTime : Float? {
        return self.chat.destructionTime
    }
    
    var isNegotiable : Bool? {
        return self.chat.negotiable
    }
    
    var productImage : URL? {
        guard let imagURL = self.chat.productImage else { return nil }
        return URL(string :imagURL)
    }
    
    var productName : String? {
        return self.chat.productName
    }
    
    var productId : String? {
        return self.chat.productId
    }
    
    var latitude : Float? {
        return self.chat.latitude
    }
    
    var longitude : Float? {
        return self.chat.longitude
    }
    
    var acceptedPrice : String? {
        return self.chat.acceptedPrice
    }
    
    var memberName : String? {
        return self.chat.membername
    }
    
    var currency : String? {
        guard let currncy = self.chat.currency else { return "" }
        guard let currenySymbol = ProductDetailsViewController().returnCurrency(currncy) else { return ""}
        return "\(currenySymbol)"
    }
    
    var prductPrice : String? {
        guard let price = self.chat.price, let currency = self.chat.currency else { return nil }
        if let currenySymbol = ProductDetailsViewController().returnCurrency(currency) {
            return "\(currenySymbol) \(price)"
        }
        return "\(currency) \(price)"
    }
    
    var isSecretInviteVisible : Bool{
        guard let isSecretInviteVisible = self.chat.isSecretInviteVisible else { return false }
        return isSecretInviteVisible
    }
    
    // EXCHANGE

    var isSwap : Bool? {
        return self.chat.isSwap
    }

    var swapType : String? {
        return self.chat.swapType
    }
    var productThumbnailImage : URL? {
        guard let imagURL = self.chat.productUrl else { return nil }
        return URL(string :imagURL)
    }

    var swapProductName : String? {
        return self.chat.swapProductName
    }

    var swapPostId : String? {
        return self.chat.swapProductId
    }

    var swapPrdocutImage : URL? {
        guard let imagURL = self.chat.swapProductUrl else { return nil }
        return URL(string :imagURL)
    }

    var swaptextMessage : String? {
        return self.chat.swapTextMessage
    }
    
    var isRejected : String?
    {
        return self.chat.isRejected
    }
    
    
    
    /// Delete chat action on this perticular chat.
    ///
    /// - Parameters:
    ///   - success: If it got succeeded then return here.
    ///   - failure: If it got failed then return here.
    func deleteChat(success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void) {
        self.deleteChatFromServer(success: { response in
            print("chat deleted")
            success(response)
            self.deleteChatLocally()
            
        }) { error in
            print("chat delete failed")
            failure(error)
            self.deleteChatLocally()
        }
    }
    
    func deleteChatLocally() {
        if let docID = self.docID {
            let individualChatVMObj = IndividualChatViewModel(couchbase: self.couchbaseObj)
            individualChatVMObj.deleteDocIDData(fromChatDocID: docID)
        }
    }
    
    func deleteChatFromServer(success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void)  {
        api.deleteChats(withSecretID: self.secretID, andRecipientID: self.senderID, success: { response in
            success(response)
        }) { error in
            print("got an error")
            failure(error)
        }
    }
    
    func getlastMessage(fromChatDocID docID : String) -> Message? {
        guard let chatData = couchbaseObj.getData(fromDocID: docID) else { return nil }
        guard let  msgArray:[Any] = chatData["messageArray"] as? [Any] else { return nil }
        if !msgArray.isEmpty {
            guard let msgObj = msgArray.last as? [String : Any] else { return nil }
            var type = "15"
            if let messageType = msgObj["messageType"] as? String {
                type = messageType
            } else if let messageType = msgObj["type"] as? String {
                type = messageType
            }
            var message = ""
            if let messageStr = msgObj["payload"] as? String {
                message = messageStr
            } else if let messageStr = msgObj["message"] as? String {
                message = messageStr
            }
            
            if type == "3" {
                let msgStr = message.replace(target: "\n", withString: "")
                let msgObj = self.getLastLocationMsg(withMsgType: type, andMessage: msgStr, andMsgObj: msgObj, docID: docID)
                return msgObj
            } else {
                let mesageObj = Message(forData: msgObj, withDocID: docID, isSelfMessage: true, andMessageobj: msgObj, offerType : offerType, isMediaIncluded: false, includedMedia: nil, withImageURL: nil)
                return mesageObj
            }
        }
        return nil
    }
    
    func getLastLocationMsg(withMsgType messageType: String, andMessage message : String, andMsgObj msgObj:[String : Any], docID : String)  -> Message? {
        var msgStr = ""
        if let msg = message.fromBase64() {
            msgStr = msg
        }else {
            msgStr = message
        }
        if let latStr = msgStr.slice(from: "(", to: ","), let longStr = msgStr.slice(from: ",", to: ")") {
            if let lat = Double(latStr), let long = Double(longStr) {
                let locationAtual: CLLocation = CLLocation(latitude: lat, longitude: long)
                let loc = JSQLocationMediaItem(location: locationAtual)
                let mesageObj = Message(forData: msgObj, withDocID: docID, isSelfMessage: true, andMessageobj: msgObj, offerType : self.offerType, isMediaIncluded: true, includedMedia: loc, withImageURL : nil)
                return mesageObj
            }
        }
        return nil
    }
    
    func getAtributeString(withMessageStatus messageStatus : String) -> NSMutableAttributedString? {
        let attribute = [NSForegroundColorAttributeName : UIColor.lightGray]
        var str = NSMutableAttributedString(string: "✔︎", attributes: attribute)
        if messageStatus == "2" {
            let attribute = [NSForegroundColorAttributeName : UIColor.gray]
            str = NSMutableAttributedString(string: "✔︎✔︎", attributes: attribute)
            return str
        }
        if messageStatus == "3" {
            let attribute = [NSForegroundColorAttributeName : UIColor.blue]
            str = NSMutableAttributedString(string: "✔︎✔︎", attributes: attribute)
            return str
        }
        return str
    }
    
    func getMessages(withTimeStamp timeStamp: String, andPageSize pageSize : String) {
        if let chatID = self.chatID {
            let msgUrl = "Messages/\(chatID)/\(timeStamp)/\(pageSize)"
            self.api.getMessages(withUrl: msgUrl)
        }
    }
    
    func getSendChatObj(withMsgObj messageObj : Message, andOfferType offerType : String)-> [String : Any]? {
        var params = [String : Any]()
        let timeStamp = UInt64(floor(Date().timeIntervalSince1970 * 1000))
        guard let userID = self.getUserID() else { return nil }
        guard let name = Helper.userName(), let payload = messageObj.messagePayload,let toID = self.recieverID,let postID = self.secretID, let userImage = self.chat.image,let chatDocID = self.docID, let productID = self.productId,let productImage = self.chat.productImage,let productName = self.productName else { return nil }
        params["name"] = name
        params["from"] = userID
        params["to"] = toID
        params["payload"] = payload.toBase64()
        params["type"] = "15"
        params["offerType"] = offerType
        params["id"] = "\(timeStamp)"
        params["secretId"] = postID
        params["thumbnail"] = ""
        params["userImage"] = userImage
        params["toDocId"] = chatDocID
        params["dataSize"] = 1
        params["isSold"] = "0" // 0 if counter 1 if accepted
        params["productImage"] = productImage
        params["productId"] = productID
        params["productName"] = productName
        params["productPrice"] = payload
        return params
    }
    
    
    func getSendChatObjForSwapOfferAccepted(withMsgObj messageObj : Message, andSwapOfferType swapOfferType : String)-> [String : Any]? {
        var params = [String : Any]()
        let timeStamp = UInt64(floor(Date().timeIntervalSince1970 * 1000))
        guard let userID = self.getUserID() else { return nil }
        guard let name = Helper.userName(), let payload = messageObj.messagePayload,let toID = self.recieverID,let postID = self.secretID, let userImage = self.chat.image,let chatDocID = self.docID, let productID = self.productId,let productImage = self.chat.productImage,let productName = self.productName else { return nil }
        params["name"] = name
        params["from"] = userID
        params["to"] = toID
        params["payload"] = payload.toBase64()
        params["type"] = "17"
        params["offerType"] = "5"
        params["id"] = "\(timeStamp)"
        params["secretId"] = postID
        params["thumbnail"] = ""
        params["userImage"] = userImage
        params["toDocId"] = chatDocID
        params["dataSize"] = 1
        params["isSold"] = "1" // 0 if counter 1 if accepted
        params["productImage"] = productImage
        params["productId"] = productID
        params["productName"] = productName
        params["productPrice"] = payload
        if(swapOfferType == "1")
        {
        params["swapType"] = "2"
        params["isRejected"] = "0"
        }
        else
        {
        params["swapType"] = "3"
        params["isRejected"] = "1"
        params["rejectMsgId"] =  messageObj.messageId
        }
        params["isSwap"] = "1";
        params["productUrl"] = messageObj.productUrl
        params["swapProductUrl"] = messageObj.swapProductUrl
        params["swapProductName"] = messageObj.swapProductName
        params["swapProductId"] = messageObj.swapProductId
        params["swapTextMessage"] = "I would like to offer"//[NSString, stringWithFormat:@"I would like to offer %@ in place of your %@",self.product.productName,swapProduct.productName];
        
        return params
    }
    
    
    
    
    fileprivate func getUserID() -> String? {
        guard let mqttID = Helper.getMQTTID() else { return nil }
        if mqttID == "mqttId" {
            return nil
        }
        return mqttID
    }
    
    func getOfferObject(withMessageObj messageObj: Message, andOfferType offerType : String,andMemberName memberName : String? , completionBlock: @escaping completionBlockType) {
        var params = [String : Any]()
        guard let price = messageObj.messagePayload, let secretID = secretID, let sendChat = self.getSendChatObj(withMsgObj: messageObj, andOfferType : offerType), let memberName = self.getRecieverName(withUserName : memberName) else { return }
        params["price"] = price as Any
        params["offerStatus"] = offerType as Any
        params["sendchat"] = sendChat as Any
        params["token"] = Helper.userToken() as Any
        params["postId"] = secretID as Any
        params["type"] = "0" as Any //  0 for image type.
        
        params["membername"] =  memberName as Any
        
        params["buyer"] = messageObj.senderDisplayName

//        if offerType == "2" {
//         params["buyer"] = name
//        }
        
        api.initiatedChat(withdata: params) { (success) in
            if let sucess = success {
                guard let responseArray = sucess["data"] as? [Any] else { return }
                guard let dataObj = responseArray[0] as? [String : Any] else { return }
                
                if let priceInString = dataObj["price"] as? String
                {
                    self.chat.acceptedPrice = priceInString
                }
                else if let priceInInt = dataObj["price"] as? Int
                {
                     self.chat.acceptedPrice = String(format: "%ld",priceInInt)
                }
                else if let priceInFloat = dataObj["price"] as? Float
                {
                     self.chat.acceptedPrice = String(format: "%0.2f",priceInFloat)
                }

                if let offerType = dataObj["offerType"] as? Int {
                    self.chat.offerType = "\(offerType)"
                } else {
                    self.chat.offerType = "3"
                }

                completionBlock(success)
            }
        }
    }
    
    
    func getSwapOfferObject(withMessageObj messageObj: Message, isSwapOfferAccepted isAccepted : String,andMemberName memberName : String? , completionBlock: @escaping completionBlockType) {
        var params = [String : Any]()
        guard let price = messageObj.messagePayload, let secretID = secretID, let sendChat = self.getSendChatObjForSwapOfferAccepted(withMsgObj: messageObj, andSwapOfferType : isAccepted)
           else { return }

        params["price"] = price as Any
        params["swapPostId"] = messageObj.swapProductId as Any
        params["sendchat"] = sendChat as Any
        params["token"] = Helper.userToken() as Any
        params["postId"] = secretID as Any
        params["type"] = "0" as Any //  0 for image type.
        params["memberName"] =  memberName as Any
        params["buyer"] = messageObj.senderDisplayName
        params["isAccepted"] = isAccepted
       
        api.AcceptSwapOffer(withdata: params) { (success) in
            if let sucess = success {
                guard let responseArray = sucess["data"] as? [Any] else { return }
                guard let dataObj = responseArray[0] as? [String : Any] else { return }
                
                if let swapOfferType = dataObj["swapType"] as? Int {
                   self.chat.swapType = "\(swapOfferType)"
                } else {
                    self.chat.swapType  = "2"
                }
                
                completionBlock(success)
            }
        }
    }
    
    
    
    
    
    
    func offerAccepted(withMessageObj messageObj: Message, andUserName userName : String?, completionBlock: @escaping completionBlockType) {
        self.getOfferObject(withMessageObj: messageObj, andOfferType: "2", andMemberName: userName, completionBlock: {_ in
            guard let receiverID = Helper.getMQTTID(), let secretID = self.secretID else { return }
            let data = ["receiverID":receiverID,
                        "secretID":secretID] as [String : String]
            if let userName = Helper.userName() {
                API().sendNotification(withText: " ", andTitle: " Congratulations \(userName) has accepted your offer.", toTopic: self.recieverID!, andData: data)
                completionBlock(data)
            }
        })
    }
    
    func swapOfferAccepted(withMessageObj messageObj: Message, andUserName userName : String?,isSwapAccepted isaccepted:String, completionBlock: @escaping completionBlockType) {
        self.getSwapOfferObject(withMessageObj: messageObj, isSwapOfferAccepted: isaccepted, andMemberName: userName, completionBlock: {_ in
            guard let receiverID = Helper.getMQTTID(), let secretID = self.secretID else { return }
            let data = ["receiverID":receiverID,
                        "secretID":secretID] as [String : String]
            if let userName = Helper.userName() {
                API().sendNotification(withText: " ", andTitle: " Congratulations \(userName) has accepted your swap offer.", toTopic: self.recieverID!, andData: data)
                completionBlock(data)
            }
        })
    }
    
    func counterOffered(withMessageObj messageObj: Message, andUserName userName : String?) {
        self.getOfferObject(withMessageObj: messageObj, andOfferType: "3", andMemberName: userName, completionBlock: { response in
            guard let receiverID = Helper.getMQTTID(), let secretID = self.secretID else { return }
            let data = ["receiverID":receiverID,
                        "secretID":secretID] as [String : String]
            if let userName = Helper.userName() {
                API().sendNotification(withText: " ", andTitle: "\(userName) has sent a counter offer on your product.", toTopic: self.recieverID!, andData: data)
            }
        })
    }
    
    func getProductDetails() {
        self.api.getPostDetailsByID(withID: self.secretID, success: { (data) in
            let chatsDocVMObject = ChatsDocumentViewModel(couchbase: self.couchbaseObj)
            self.updateChatDataLocaly(withData: data)
            chatsDocVMObject.updateYeloChatRelatedData(withData: data, andChatObj: self.chat)
        }) { (error) in
            print(error)
            if error.code  == 204, let chatDocID = self.docID {
                if var chatData = self.couchbaseObj.getData(fromDocID: chatDocID) {
                    self.chat.isProductSold = true
                    chatData["productIsSold"] = 1 as Any
                    self.couchbaseObj.updateData(data: chatData, toDocID: chatDocID)
                }
            }
        }
    }
    
    func updateDataForReceiverProfilePic (product:ProductDetails) {
        let chatsDocVMObject = ChatsDocumentViewModel(couchbase: self.couchbaseObj)
        chatsDocVMObject.updateProfilePicFOrReceiver(productDetails: product, andChatObj: self.chat)
    }

    
    func updateChatDataLocaly(withData data:[String : Any]) {
        if  let memberName = data["membername"] as? String,let productImage = data["mainUrl"] as? String, let currency = data["currency"] as? String, let productName = data["productName"] as? String, let place = data["place"] as? String, let receiverImage = data["profilePicUrl"] as? String, let negotiable = data["negotiable"] as? Bool{
            chat.membername = memberName
            if let latitude = data["latitude"] as? Float
            {
                chat.latitude = latitude
            }
            if let longitude = data["longitude"] as? Float
            {
                chat.longitude = longitude
            }
            chat.productImage = productImage
            chat.currency = currency
            chat.productName = productName
            chat.place = place
            chat.profilePicUrl = receiverImage
            chat.negotiable = negotiable
            
            if let priceInString = data["price"] as? String
            {
                chat.price  = priceInString
            }
            else if let priceInInt = data["price"] as? Int
            {
                chat.price = String(format: "%ld",priceInInt)
            }
            else if let priceInFloat = data["price"] as? Float
            {
                chat.price = String(format: "%0.2f",priceInFloat)
            }
        }
        //        let name = NSNotification.Name(rawValue: "ChatDataUpdatedNotification")
        //        NotificationCenter.default.post(name: name, object: self, userInfo: nil)
    }
    
    func getRecieverName(withUserName usrName : String?) -> String? {
        if let isInitiated = self.initiated {
            if !isInitiated {
                return Helper.userName()
            } else {
                if let name = usrName {
                    if name == self.name {
                        return self.memberName
                    } else {
                        return name
                    }
                }
            }
        }
        return self.memberName
    }
}

