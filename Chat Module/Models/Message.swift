
//
//  Message.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 11/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

import Kingfisher


/// enum for the type of messages available in the chat.
///
/// - text: text type
/// - image: image type
/// - video: video type
/// - location: location type
/// - contact: contact type
/// - audio: audio type
/// - sticker: sticker type
/// - doodle: doodle type
/// - gif: gif type
/// - header: header type
/// - order: order type
/// - payment: payment type
enum MessageTypes:Int {
    case text = 0
    case image = 1
    case video = 2
    case location = 3
    case contact = 4
    case audio = 5
    case sticker = 6
    case doodle = 7
    case gif = 8
    case header = 9
    case order = 15
    case payment = 16
    case swap  = 17
}


/// Used as Message model inheriting message properties.
class Message: JSQMessage {
    
    
    /// keys constants used in model.
    struct KeyConstants {
        
        /// from key
        static let from = "from"
        
        /// to Key
        static let to = "to"
        
        /// payload key
        static let payload = "payload"
        
        /// docId key
        static let docId = "toDocId"
        
        /// timeStamp key
        static let timeStamp = "timestamp"
        
        /// messageType key
        static let messageType = "type"
        
        /// messangerName key
        static let messangerName = "name"
        
        /// messageID key
        static let messageID = "id"
        
        /// msgSentDate key
        static let msgSentDate = "sentDate"
        
        /// msgStatus key
        static let msgStatus = "deliveryStatus"
        
        /// isSelfMessage key
        static let isSelfMessage = "isSelf"
    }
    
    
    /// message From ID
    var messageFromID : String?
    
    /// message To ID
    var messageToID : String?
    
    /// message Payload
    var messagePayload : String?
    
    /// Type of message
    var messageType : MessageTypes?
    
    /// message Doc Id
    var messageDocId : String?
    
    /// timestamp of message
    var timeStamp : String?
    
    /// messanger Name
    var messangerName : String?
    
    /// message Sent Date
    var messageSentDate : Date?
    
    /// message Status
    var messageStatus : String?
    
    /// is Self Message
    var isSelfMessage : Bool!
    
    /// unique message Id
    var uniquemessageId : Int64?
    
    /// if it is Self Message
    var isMediaAvailable : Bool!
    
    /// image Url if available
    var imageUrl : String?
    
    /// offer Type of the message
    var offerType : String?
    
    // Exchange

    /// Is swap Offer.
    var isSwap : Bool?

    /// Swap type.
    var swapType : String?

    /// thumbnail image of product.
    var productUrl : String?

    /// Swap product name.
    var swapProductName : String?

    /// swap product thumnail url.
    var swapProductUrl : String?

    /// post id of swap product.
    var swapProductId : String?

    /// text message of swap product.
    var swapTextMessage : String?
    
    var isRejected :String?
    
    /// Used for instantiating the message object.
    ///
    /// - Parameters:
    ///   - messageFromID: message From user ID
    ///   - messageToID: message To user ID
    ///   - messagePayload: Message string
    ///   - messageDocId: message Doc Id
    ///   - timeStamp: timeStamp
    ///   - messageType: message Type
    ///   - messangerName: messanger Name
    ///   - messageSentDate: message Sent Date
    ///   - messageId: unique message Id
    ///   - media: media
    ///   - isMediaAvailable: is Media Available if available then its true else false
    ///   - messageStatus: message delivery Status
    ///   - isSelfMessage: if it is Self Message
    ///   - imageUrl: image Url if available
    ///   - offerType: offer Type of the message
    init(withSenderID messageFromID : String, andRecieverID messageToID : String?, withPayload messagePayload : String ,messageDocId : String?, timeStamp : String? , messageType : MessageTypes?, messangerName : String, messageSentDate : Date,messageId: String, media: JSQMessageMediaData?,isMediaAvailable : Bool, messageStatus : String, isSelfMessage : Bool, imageUrl : String?, offerType : String?,isSwap : Bool? ,swapType:String?,productUrl:String?,swapProductName:String?,swapProductUrl:String?,swapProductId:String?,swapTextMessage:String?,isRejectedStatus:String?) {
    
        if isMediaAvailable {
            super.init(senderId: messageFromID, senderDisplayName: messangerName, date: messageSentDate, media: media!, messageId: messageId)
        } else {
            super.init(senderId: messageFromID, senderDisplayName: messangerName, date: messageSentDate, text: messagePayload, messageId: messageId)
        }
        
        self.messageFromID = messageFromID
        self.messageSentDate = messageSentDate
        self.messangerName = messangerName
        self.messagePayload = messagePayload
        self.messageToID = messageToID
        self.messageDocId = messageDocId
        self.timeStamp = timeStamp
        self.messageType = messageType
        self.messangerName = messangerName
        self.messageStatus = messageStatus
        self.isSelfMessage = isSelfMessage
        self.isMediaAvailable = isMediaAvailable
        if let uniquemessageId = timeStamp{
            self.uniquemessageId = Int64(uniquemessageId)
        }
        self.offerType = offerType
        self.imageUrl = imageUrl
        
        self.isSwap = isSwap
        self.swapType = swapType
        self.productUrl = productUrl
        self.swapProductName = swapProductName
        self.swapProductUrl = swapProductUrl
        self.swapProductId = swapProductId
        self.swapTextMessage = swapTextMessage
        self.isRejected = isRejectedStatus
        
    }
    
    /// Used for initializing the Message modal object by the custom properties.
    ///
    /// - Parameters:
    ///   - data: message data in dictionary format
    ///   - docID: current chat doc ID
    ///   - isSelf: if the message is from self
    ///   - messageObj: if message object is available
    ///   - offerType: current message offer type
    ///   - isMediaIncluded: if the media is included
    ///   - includedMedia: included media in JSQMedia format.
    convenience init(forData data : [String:Any], withDocID docID : String,isSelfMessage isSelf : Bool, andMessageobj messageObj : [String:Any], offerType : String?, isMediaIncluded : Bool, includedMedia : JSQMessageMediaData?, withImageURL imageURL: String?) {
        
        let data = data
        var payload = ""
        var timeStamp = "\(DateExtension().sendTimeStamp(fromDate:Date())!)"
        var type = ""
        var isMediaAvailable : Bool = false
        var msgStatus = "0"
        var messageID = ""
        var media: JSQMessageMediaData?
        let senderID = data[KeyConstants.from] as? String
        let recieverId = data[KeyConstants.to] as? String
        let messangerName = messageObj[KeyConstants.messangerName] as? String
        if let msgID = data[KeyConstants.messageID] as? String {
            messageID = msgID
        } else if let  msgID = data[KeyConstants.messageID] as? Int{
            messageID = "\(msgID)"
        }
        if let messageStatus = data[KeyConstants.msgStatus] as? String {
            msgStatus = messageStatus
        }
        
        var isSelfMessage : Bool = isSelf
        if let isSelfMessageFlag = data[KeyConstants.isSelfMessage] as? Bool {
            isSelfMessage = isSelfMessageFlag
            
            var offerSenderId = ""
            if let messageSenderId = messageObj["senderId"] as? String
            {
                offerSenderId = messageSenderId
            }
            
            
            if let selfID =  Helper.getMQTTID(),let senderId = messageObj["from"] as? String {
                if selfID == senderId || selfID == offerSenderId {
                    isSelfMessage = true
                }
                else
                {
                    if !offerSenderId.isEmpty
                    {
                      isSelfMessage = false
                    }
                }
            }
            
        }
        
        var isSwap = false
        var swapType = "2"
        var productUrl = ""
        var swapProductName = ""
        var swapProductUrl = ""
        var swapProductId = ""
        var swapTextMessage = ""
        var isRejected = ""
        
        if  let isswap = data["isSwap"] as? String,
            let swaptype = data["swapType"] as? String,
            let productThumbUrl = data["productUrl"] as? String,
            let swapProductTitle = data["swapProductName"] as? String,
            let swapProductImage = data["swapProductUrl"] as? String,
            let swapText = data["swapTextMessage"] as? String,
            let swapPostId = data["swapProductId"] as? String,
            let isRejectedStatus = data["isRejected"] as? String
        {
            if(isswap == "1")
            {
               isSwap = true
            }
            else
            {
               isSwap = false
            }
            if(offerType == "5"){
               swapType = "2"
            }
            else
            {
            swapType = swaptype
            }
            productUrl = productThumbUrl
            swapProductName = swapProductTitle
            swapProductUrl = swapProductImage
            swapProductId  = swapPostId
            swapTextMessage = swapText
            isRejected  = isRejectedStatus
            
        }
        
        
        
        if  let isswap = data["isSwap"] as? Bool,
            let swaptype = data["swapType"] as? String,
            let productThumbUrl = data["productUrl"] as? String,
            let swapProductTitle = data["swapProductName"] as? String,
            let swapProductImage = data["swapProductUrl"] as? String,
            let swapText = data["swapTextMessage"] as? String,
            let swapPostId = data["swapProductId"] as? String,
            let isRejectedStatus = data["isRejected"] as? String
        {
            isSwap = isswap
            if(offerType == "5"){
                swapType = "2"
            }
            else
            {
                swapType = swaptype
            }
            productUrl = productThumbUrl
            swapProductName = swapProductTitle
            swapProductUrl = swapProductImage
            swapProductId  = swapPostId
            swapTextMessage = swapText
            isRejected  = isRejectedStatus
            
        }
        
        
        
        
        
        
        if let message = messageObj[KeyConstants.payload] as? String{
            payload = message.replace(target: "\n", withString: "")
        } else if let message = messageObj["message"] as? String {
            payload = message.replace(target: "\n", withString: "")
        }
        if payload == "" {
            payload = "There is no message to show"
        }
        
        if let ts = messageObj[KeyConstants.timeStamp] as? String{
            timeStamp = ts
        } else if let ts = data["timestamp"] as? String {
            timeStamp = ts
        } else if let ts = messageObj[KeyConstants.timeStamp] as? Int64 {
            timeStamp = "\(ts)"
        }
        
        if let messageType = messageObj[KeyConstants.messageType] as? String {
            type = messageType
        } else if let messageType = messageObj["messageType"] as? String {
            type = messageType
        } else if let messageType = messageObj[KeyConstants.messageType] as? Int {
            type = "\(messageType)"
        }
        
        if type == ""
        {
            type = "0"
            timeStamp = "1542090029565"
        }

        
        switch type {
        case "0": // for text messages.
            isMediaAvailable = false
            
        case "1" : //For Image
            isMediaAvailable = true
            if isSelfMessage {
                media = SentImageMediaItem()
            } else {
                media = ReceivedImageMediaItem()
            }

        case "3" : //Location
            isMediaAvailable = true
            if isSelfMessage {
                media = SentLocationMediaItem()
            } else {
                media = ReceivedLocationMediaItem()
            }
            
        case "15" : // Offer
            isMediaAvailable = true
            if offerType == "2" {
                if isSelfMessage {
                    //1 case for isSelf and Offer = 2, is Accepted
                    media = OfferSentMediaItem()
                } else {             //2 case for !isSelf and Offer = 2, is Accepted
                    media = OfferRecievedMediaItem()
                }
            } else if offerType == "4"{
                if isSelfMessage {
                    media = AcceptedMediaItem() // If user has accepted the offer
                } else {
                    media = ThankYouMessageMediaItem() // For showing thank you message
                }
            } else {
                if isSelfMessage {             //1 case for isSelf and Offer = 1,3 is Not Accepted
                    media = OfferSentMediaItem()
                } else {             //1 case for !isSelf and Offer = 1,3  is Not Accepted
                    media = CounterOfferCellMediaItem()
                }
            }
            
        case "17" : // Swap
            isMediaAvailable = true
            if swapType == "4" && !isSelfMessage {
                
                if(isRejected == "0")
                {
                    media = SwapOfferRecievedMediaItem()
                }
                else
                {
                   media = SwapOfferAcceptedMediaItem()
                }
                
               
            }
            else if swapType == "5" && !isSelfMessage
            {
                media = SwapOfferAcceptedMediaItem()
            }
            else if swapType == "2" || swapType == "3"
            {
              media = ThankYouMessageMediaItem()
            }
            else  {
               if isSelfMessage {
                    media = SwapOfferSentMediaItem() // If user has sent swap offer
                } else {
                    media = SwapOfferRecievedMediaItem() //if user has recieved swap offer
                 }
                }
        case "16" : //Paypal
            
            if isSelfMessage {
                isMediaAvailable = true
                media = PayPalLinkSharedCellMediaItem()
            } else {
                isMediaAvailable = true
                media = PaypalCellMediaItem()
            }
            
        default:
            isMediaAvailable = false
        }
        
        let messageTypeObj:MessageTypes = MessageTypes(rawValue: Int(type)!)!
        let messageSendDate : Date = DateExtension().getDateObj(fromTimeStamp: timeStamp)
        var decodMsg = ""
        if let decodedMsg = payload.fromBase64() {
            decodMsg = decodedMsg
        } else {
            decodMsg = payload
        }
        if isMediaIncluded {
            media = includedMedia
        }
       
        
        self.init(withSenderID: senderID!, andRecieverID: recieverId, withPayload: decodMsg, messageDocId: docID, timeStamp: timeStamp, messageType: messageTypeObj, messangerName: messangerName!, messageSentDate: messageSendDate, messageId: messageID, media: media, isMediaAvailable: isMediaAvailable, messageStatus: msgStatus, isSelfMessage: isSelfMessage, imageUrl : imageURL, offerType : offerType,isSwap :isSwap ,swapType:swapType,productUrl:productUrl,swapProductName:swapProductName,swapProductUrl:swapProductUrl,swapProductId:swapProductId,swapTextMessage:swapTextMessage,isRejectedStatus:isRejected)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
