//
//  paypalButtonTableViewCell.h

//
//  Created by Imma Web Pvt Ltd on 09/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface paypalButtonTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lableoutlet;
@property (weak, nonatomic) IBOutlet UIButton *paypalButtonOutlet;

@end
