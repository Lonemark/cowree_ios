//
//  PayPalSectionHeaderCell.m
//  Yelo
//
//  Created by Rahul Sharma on 02/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "PayPalSectionHeaderCell.h"

@implementation PayPalSectionHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
