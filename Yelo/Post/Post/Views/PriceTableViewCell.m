//
//  PriceTableViewCell.m
//  Vendu
//
//  Created by Rahul Sharma on 19/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "PriceTableViewCell.h"
#import "CurrencySelectVC.h"

@interface PriceTableViewCell()<CurrencyDelegate , UITextFieldDelegate>

@end

@implementation PriceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setPrice :(NSString *)price  currency :(NSString *)currency
{
    self.currency.text = currency ;
    self.priceTextField.text = price ;
}

- (IBAction)currencyButtonAction:(id)sender {
    
    CurrencySelectVC *currencySelectVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CurrencySelectVC"];
    
    [currencySelectVC setDelegate:self];
    currencySelectVC.previousSelection = self.currency.text ;
    [self.refrenceVC.navigationController pushViewController:currencySelectVC animated:YES];
}

- (IBAction)priceTextFieldValueChangedAction:(id)sender {
    
    self.listing.price =  flStrForObj(self.priceTextField.text);
}


#pragma mark - CurrencyDelegate Method
-(void) country:(CurrencySelectVC *)country didChangeValue:(id)value{
    [country setDelegate:nil];
    NSDictionary *countryDict = value;
    self.currency.text=[NSString stringWithFormat:@" %@  %@",[countryDict objectForKey:CURRENCY_CODE],[countryDict objectForKey:CURRENCY_SYMBOL]];
   self.refrenceVC.setCurrency = self.currency.text;
    self.listing.currency = [NSString stringWithFormat:@"%@",[countryDict objectForKey:CURRENCY_CODE]];
}

#pragma mark - TextField Delegate -

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.refrenceVC.tapGestureOutlet.enabled = YES ;
    *self.isPriceTextFieldEnable = YES ;
}

-(void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason
{
    self.listing.price = self.priceTextField.text ;
     self.refrenceVC.tapGestureOutlet.enabled = NO ;
    *self.isPriceTextFieldEnable = NO ;
}


@end
