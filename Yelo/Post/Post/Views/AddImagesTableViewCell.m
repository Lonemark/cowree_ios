//
//  AddImagesTableViewCell.m
//  Vendu
//
//  Created by Rahul Sharma on 19/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "AddImagesTableViewCell.h"
#import "MultipleImagesCell.h"
#import "CameraViewController.h"
#import "JTSImageViewController.h"
#import "JTSImageInfo.h"

@interface AddImagesTableViewCell() <cameraScreenDelegate , UITextFieldDelegate>
@end


@implementation AddImagesTableViewCell 

- (void)awakeFromNib {
    [super awakeFromNib];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMaintainedDataOfImagesFromLibrary:)name:mLibraryNotification object:nil];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


/*------------------------------*/
#pragma mark- CollectionView DataSource Method
/*------------------------------*/

-(NSInteger )collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if(_arrayOfImagePaths.count<5){
        return self.arrayOfImagePaths.count+1;
    }
    else{
        return self.arrayOfImagePaths.count;
    }
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    MultipleImagesCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:mAddMultiImagesCellID forIndexPath:indexPath];
     UIImage *containerImage;
    if( indexPath.row == self.arrayOfImagePaths.count)
    {
        [cell updateCellObjectWithImage:[UIImage imageNamed:mAddImageButtonImageName]];
        return cell;
    }
    
    
    if([self.arrayOfImagePaths[indexPath.row][@"imageType"] isEqualToString:@"cloudinaryUrl"])
    {   UIImageView *tempImageView = [[UIImageView alloc]init];
        
        containerImage =  [tempImageView image];
        containerImage =  [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.arrayOfImagePaths[indexPath.row][@"imageValue"]]]];
    }
    else
    {
        NSString *filePath = flStrForObj(self.arrayOfImagePaths[indexPath.row][@"imageValue"]);
        NSData *imgData = [NSData dataWithContentsOfFile:filePath];
        containerImage = [[UIImage alloc] initWithData:imgData];
    }
    
    
    NSNumber *width = [NSNumber numberWithFloat:containerImage.size.width];
    NSNumber *height= [NSNumber numberWithFloat:containerImage.size.height];
    [arrayOfContainerWidths addObject:width];
    [arrayOfContainerHeights addObject:height];
    [cell updateCellWithImage:containerImage andIndex:indexPath];
    [cell.removeButton addTarget:self action:@selector(removeImage:) forControlEvents:UIControlEventTouchUpInside];
    
     self.labelForCount.text = [NSString stringWithFormat:@"Add upto %u more photos (optional)",(unsigned)(5 - self.arrayOfImagePaths.count)];
    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize cellSize;
    cellSize.width = mWidthOfAddMultiImagesCell ;
    cellSize.height = mHeightOfAddMultiImagesCell;
    return cellSize;
    
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if(collectionView.tag == 100)
    {
        if(indexPath.row == self.arrayOfImagePaths.count)
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            CameraViewController *cameraVC  = [storyboard instantiateViewControllerWithIdentifier:@"CameraStoryBoardID"];
            cameraVC.cameraAgain = true;
            cameraVC.arrayOfChoosenImages = self.arrayOfImagePaths ;
            cameraVC.cameraDelegate = self;
            [self.referenceVC.navigationController pushViewController:cameraVC animated:NO];
        }
        
        else
        {
            [self tapToSeeFullImage:indexPath.row];
        }
        
    }
}

/*-------------------------------------*/
#pragma mark
#pragma mark - RemoveImage Selector Method.
/*------------------------------------*/

-(void)removeImage:(id) sender
{
    UIButton *btn=(UIButton *)sender;
    NSInteger tag=btn.tag%1000;
    [self.arrayOfImagePaths removeObjectAtIndex:tag];
    self.listing.arrayOfImagePaths = self.arrayOfImagePaths;
    [self.collectionViewForImages reloadData];
    int imageCount = (unsigned)(self.arrayOfImagePaths.count);
    self.labelForCount.text = [NSString stringWithFormat:@"Add upto %u more photos (optional)",(unsigned)(5 - imageCount)];
    if(self.arrayOfImagePaths.count==0)
    {
        self.labelForCount.text = NSLocalizedString(addAtleastSingleImage, addAtleastSingleImage);
    }
}


/*--------------------------------------*/
#pragma mark
#pragma mark - tapGesture.
/*--------------------------------------*/

-(void)tapToSeeFullImage :(NSInteger)index
{ UIImageView *fullImageView = [[UIImageView alloc]init];
    JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
    
    if([self.arrayOfImagePaths[index][@"imageType"] isEqualToString:@"cloudinaryUrl"]){
        NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:self.arrayOfImagePaths[index][@"imageValue"]]];
        fullImageView.image = [UIImage imageWithData: imageData];
    }
    else
    {
        NSString *filePath = flStrForObj(self.arrayOfImagePaths[index][@"imageValue"]);
        NSData *imgData = [NSData dataWithContentsOfFile:filePath];
        fullImageView.image = [[UIImage alloc]initWithData:imgData];
        
    }
    
    imageInfo.image = fullImageView.image;
    imageInfo.referenceRect = fullImageView.frame;
    imageInfo.referenceView = fullImageView.superview;
    imageInfo.referenceContentMode = fullImageView.contentMode;
    imageInfo.referenceCornerRadius = fullImageView.layer.cornerRadius;
    
    JTSImageViewController *imageViewer = [[JTSImageViewController alloc]
                                           initWithImageInfo:imageInfo
                                           mode:JTSImageViewControllerMode_Image
                                           backgroundStyle:JTSImageViewControllerBackgroundOption_None];
    [imageViewer showFromViewController:self.referenceVC transition:JTSImageViewControllerTransition_FromOriginalPosition];
    
}

#pragma mark -
#pragma mark - CameraScreen Delegate

-(void)getMaintainedDataOfImages:(NSMutableArray *)arrayOfmaintainedImages
{
     self.arrayOfImagePaths = arrayOfmaintainedImages;
     [self.collectionViewForImages reloadData];
    self.listing.arrayOfImagePaths  = self.arrayOfImagePaths;
}

#pragma mark -
#pragma mark - Library Delegate

-(void)getMaintainedDataOfImagesFromLibrary:(NSNotification *)noti
{
    self.arrayOfImagePaths = noti.object;
    [self.collectionViewForImages reloadData];
    self.listing.arrayOfImagePaths  = self.arrayOfImagePaths;
}

#pragma mark - UITextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.referenceVC.tapGestureOutlet.enabled = YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.listing.titleOfPost = self.titleTextField.text ;
    self.referenceVC.tapGestureOutlet.enabled = NO;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if(newLength > 70)
    {
        [Helper showAlertWithTitle:nil Message:NSLocalizedString(@"Title should be less than 70 characters", @"Title should be less than 70 characters") viewController:self.referenceVC];
    }
    return newLength <= 70;
}


@end
