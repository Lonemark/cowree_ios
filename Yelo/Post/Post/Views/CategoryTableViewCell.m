//
//  CategoryTableViewCell.m
//  Vendu
//
//  Created by Rahul Sharma on 19/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "CategoryTableViewCell.h"
#import "CategoriesListTableViewController.h"
@implementation CategoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setTitle:(NSString *)title andValue :(NSString *)value
{
    self.title.text = title ;
    self.value.text = value ;
    
}

- (IBAction)categoryButtonAction:(id)sender
{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    CategoriesListTableViewController *listVC = [storyboard instantiateViewControllerWithIdentifier:@"listTableView"];
    listVC.navigationItem.title = NSLocalizedString(self.title.text, self.title.text);
    listVC.listing = self.listing;

    if ([self.title.text isEqualToString:NSLocalizedString(navTitleForProductCategory, navTitleForProductCategory)])
    {
        listVC.showResultsFor = @"categories";
    }
    else
    {
        listVC.showResultsFor = @"condition";
    }
    listVC.previousSelection = self.value.text;
    listVC.callBack = ^(Category *category,NSString *condition)
    {
        if ([self.title.text isEqualToString:NSLocalizedString(navTitleForProductCategory, navTitleForProductCategory)])
        {
            self.value.text = category.name;
            self.listing.category = self.value.text ;
        }
        else
        {
            self.value.text = condition;
            self.listing.condition = self.value.text ;
        }
    };
    [self.referenceVC.navigationController pushViewController:listVC animated:YES];

}

@end
