//
//  ExchangeListTableViewCell.h
//  Tac Traderz
//
//  Created by 3Embed on 04/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostListingsViewController.h"

@interface ExchangeListTableViewCell : UITableViewCell
 <UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate, WebServiceHandlerDelegate>

{
    NSMutableArray *postSuggessionArray, *swapListArray, *suggessionArray;
}

@property(nonatomic)BOOL isProductDeatils;
@property(nonatomic, strong)NSArray *swapPostsArray;
@property (nonatomic,strong) Listings *listing ;
@property (nonatomic,strong) PostListingsViewController *referenceVC;
@property (weak, nonatomic) IBOutlet UITextField *sugessionTextField;
@property (weak, nonatomic) IBOutlet UICollectionView *swapListCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *suggestionCollectionView;

- (IBAction)textFieldValueChanged:(id)sender;
- (IBAction)removeSwapPostButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *negotiable;
@property (weak, nonatomic) IBOutlet UILabel *price;

-(void)setPriceForProduct :(ProductDetails *)product;


@end
