//
//  SwapListCollectionViewCell.m
//  Tac Traderz
//
//  Created by 3Embed on 04/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "SwapListCollectionViewCell.h"

@implementation SwapListCollectionViewCell

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.containerView.layer.borderColor = mBaseColor.CGColor;
}


-(void)setSwapPostsWithdata:(NSArray *)swapPostArray forIndexPath:(NSIndexPath *)indexPath
{
    PostSuggession *post = swapPostArray[indexPath.row];
    self.swapProductName.text = post.productName;
    self.removeButtonOutlet.tag = indexPath.row;
    self.swapProductName.textColor = mBaseColor;
    self.containerView.layer.borderColor = mBaseColor.CGColor;
    self.imageView.image = [UIImage imageNamed:@"remove_Filters"];
}
-(void)addMoreSwapPostsButton :(NSIndexPath *)indexPath
{
    self.swapProductName.text = @"Add";
    self.swapProductName.textColor = mBaseColor2;
    self.removeButtonOutlet.tag = indexPath.row;
    self.containerView.layer.borderColor = mBaseColor2.CGColor;
    self.imageView.image = [UIImage imageNamed:@"add_swap_list"];
}
@end
