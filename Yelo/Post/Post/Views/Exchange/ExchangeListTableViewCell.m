//
//  ExchangeListTableViewCell.m
//  Tac Traderz
//
//  Created by 3Embed on 04/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "ExchangeListTableViewCell.h"
#import "SwapListCollectionViewCell.h"
#import "PostSugessionCollectionViewCell.h"
#import "PostSuggession.h"
#import "SwapSuggessionViewController.h"

@implementation ExchangeListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    postSuggessionArray = [NSMutableArray new];
    swapListArray = [NSMutableArray new];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


-(void)setPriceForProduct :(ProductDetails *)product
{
    self.price.text = [NSString stringWithFormat:@"%@%@",[Helper returnCurrency:product.currency],product.price];
    if(!product.negotiable)
    {
        self.negotiable.text = NSLocalizedString(productNotNegotiable, productNotNegotiable);
    }else
    {
        self.negotiable.text = NSLocalizedString(productNegotiable, productNegotiable);
        
    }
}

/*------------------------------*/
#pragma mark- CollectionView DataSource Method
/*------------------------------*/

-(NSInteger )collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if(_isProductDeatils)
    {
        return self.swapPostsArray.count;
    }
    return self.listing.swapPostArray.count + 1;
    
    //    return postSuggessionArray.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(collectionView.tag == 0)
    {
        SwapListCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"swapListCollectionCell" forIndexPath:indexPath];
        
        if(!_isProductDeatils && indexPath.row == self.listing.swapPostArray.count)
        {
            [cell addMoreSwapPostsButton:indexPath];
            return cell;
        }
        
        if(_isProductDeatils)
        { cell.removeButtonOutlet.hidden = YES;
            [cell setSwapPostsWithdata:self.swapPostsArray forIndexPath:indexPath];
        }
        else
        {
            [cell setSwapPostsWithdata:self.listing.swapPostArray forIndexPath:indexPath];
        }
        return cell;
    }
    else
    {
        PostSugessionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"postSuggessionCell" forIndexPath:indexPath];
        PostSuggession *post = postSuggessionArray[indexPath.row];
        cell.postsuggessionName.text = post.productName;
        return cell;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    PostSuggession *post;
    CGSize size;
    size.width = 100;
    if (self.swapPostsArray.count){
        post = self.swapPostsArray[indexPath.row];
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:RobotoRegular size:14]};
        size =  [post.productName sizeWithAttributes:attributes];
    }
    
    if (self.listing.swapPostArray.count && indexPath.row != self.listing.swapPostArray.count) {
        post = self.listing.swapPostArray[indexPath.row];
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:RobotoRegular size:14]};
        size =  [post.productName sizeWithAttributes:attributes];
    }
    
    if(collectionView.tag == 0)
    {
        return CGSizeMake(size.width + 30, 30);
    }
    CGSize cellSize;
    cellSize.width = 100 ;
    cellSize.height = 30;
    return cellSize;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    //return UIEdgeInsetsMake(5,5,5,5);
    return UIEdgeInsetsZero;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(collectionView.tag == 0){
        
        if(!_isProductDeatils && indexPath.row == self.listing.swapPostArray.count )
        {
            SwapSuggessionViewController *newVC = [self.referenceVC.storyboard instantiateViewControllerWithIdentifier:@"swapSuggessionStoryboard"];
            newVC.listing = self.listing;
            newVC.refrenceVC = self.referenceVC;
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newVC];
            [self.referenceVC presentViewController:navigationController animated:YES completion:nil];
        }
        
    }  
}


#pragma mark - UITextField Delegate


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.referenceVC.view endEditing:YES];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    //self.referenceVC.tapGestureOutlet.enabled = YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    // self.referenceVC.tapGestureOutlet.enabled = NO;
}


- (IBAction)textFieldValueChanged:(id)sender {
    
    NSDictionary *requestDict = @{mauthToken:[Helper userToken],
                                  @"productName":flStrForObj(self.sugessionTextField.text),
                                  };
    
    [WebServiceHandler getProductSuggesstion:requestDict andDelegate:self];
    
    
}

#pragma mark - WebService Delegate

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    if (error) {
        
        return;
    }
    
    if(requestType == RequestPostSuggesstion)
    {
        switch ([response[@"code"] integerValue]) {
            case 200: {
                [postSuggessionArray removeAllObjects];
                postSuggessionArray = [PostSuggession arrayOfPostSuggessions:response[@"data"]];
                [self.suggestionCollectionView  reloadData];
            }
                break;
            default:
                break;
        }
    }
    
}


- (IBAction)removeSwapPostButtonAction:(id)sender {
    
    UIButton *removeButton = (UIButton *)sender;
    if(removeButton.tag != self.listing.swapPostArray.count)
    {
        [self.listing.swapPostArray removeObjectAtIndex:removeButton.tag];
        // self.listing.swapPostArray = swapListArray;
        [self.swapListCollectionView reloadData];
    }
}
@end
