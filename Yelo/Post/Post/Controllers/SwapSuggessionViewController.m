//
//  SwapSuggessionViewController.m
//  Tac Traderz
//
//  Created by 3Embed on 07/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "SwapSuggessionViewController.h"
#import "SwapSugessionTableViewCell.h"
#import "SwapListCollectionViewCell.h"
#import "PostSuggession.h"

@interface SwapSuggessionViewController ()<UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, WebServiceHandlerDelegate>
{
    NSMutableArray *postSuggessionArray,*swapListArray;
}

@end

@implementation SwapSuggessionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    postSuggessionArray = [NSMutableArray new];
    swapListArray = [NSMutableArray new];
    self.searchBarOutlet.text = @"";
    [_searchBarOutlet becomeFirstResponder];
    
    [swapListArray addObjectsFromArray:self.listing.swapPostArray];
    //swapListArray = self.listing.swapPostArray;
    if(!swapListArray.count)
    {
        self.heightConstraintOfCollectionView.constant = 0;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SearchBar Delegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if(searchBar.text.length){
        NSDictionary *postDict = @{
                                   @"description" : @"Manual Entry",
                                   @"mainUrl" :@"",
                                   @"postId" : @"",
                                   @"postedOn" : @"",
                                   @"productName" : searchBar.text
                                   };
        
        PostSuggession *manualSuggestion =[[PostSuggession alloc]initWithDictionary:postDict];
        [swapListArray addObject:manualSuggestion];
        [self.swapListCollectionView reloadData];
        self.heightConstraintOfCollectionView.constant = 50;
        self.searchBarOutlet.text = @"";
    };
    [searchBar resignFirstResponder];
}


- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    return YES;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    NSString *encodedString =  [searchText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    if (![searchText isEqualToString:@""]) {
        NSDictionary *requestDict = @{mauthToken:[Helper userToken],
                                      @"productName":flStrForObj(encodedString),
                                      };
        [WebServiceHandler getProductSuggesstion:requestDict andDelegate:self];
    }
}



#pragma mark - TableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return postSuggessionArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SwapSugessionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"swapSuggessionCell"];
    PostSuggession *post = postSuggessionArray[indexPath.row];
    cell.postSuggesstionName.text = post.productName;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    bool isAlreadyInList = NO;
    PostSuggession *suggesstion = postSuggessionArray[indexPath.row];
    for (PostSuggession *post in swapListArray)
    {
        if([post.postId isEqualToString:suggesstion.postId])
        {
            isAlreadyInList = YES;
            break;
        }
    }
    if(!swapListArray.count)
    {
        self.heightConstraintOfCollectionView.constant = 50;
    }
    if(!isAlreadyInList)
    {
        [swapListArray addObject:postSuggessionArray[indexPath.row]];
    }
    [self.swapListCollectionView reloadData];
}


/*------------------------------*/
#pragma mark- CollectionView DataSource Method
/*------------------------------*/

-(NSInteger )collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return swapListArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SwapListCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"swapListCollectionCell" forIndexPath:indexPath];
    PostSuggession *post = swapListArray[indexPath.row];
    cell.swapProductName.text = post.productName;
    cell.removeButtonOutlet.tag = indexPath.row;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    PostSuggession *post;
    CGSize size;
    size.width = 100;
    if (swapListArray.count){
        post = swapListArray[indexPath.row];
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:RobotoRegular size:14]};
        size =  [post.productName sizeWithAttributes:attributes];
    }
    
    if(collectionView.tag == 0)
    {
        return CGSizeMake(size.width + 30, 30);
    }
    CGSize cellSize;
    cellSize.width = 100 ;
    cellSize.height = 30;
    return CGSizeMake(size.width + 30, 30);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5,5,5,5);
}


#pragma mark - WebService Delegate

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    if (error) {
        
        return;
    }
    
    if(requestType == RequestPostSuggesstion)
    {
        switch ([response[@"code"] integerValue]) {
            case 200: {
                [postSuggessionArray removeAllObjects];
                postSuggessionArray = [PostSuggession arrayOfPostSuggessions:response[@"data"]];
                [self.suggessionTableView  reloadData];
            }
                break;
            default:
                break;
        }
    }
    
}


- (IBAction)cancelButtonAction:(id)sender {
    
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveButtonAction:(id)sender {
    
    self.listing.swapPostArray = swapListArray;
    
    NSIndexPath* rowToreload  = [NSIndexPath indexPathForRow:7 inSection:0];
    
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToreload, nil];
    
    [self.refrenceVC.tableViewForListings reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationFade];
    
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)removeSwapPostButtonAction:(id)sender {
    UIButton *removeButton = (UIButton *)sender;
    [swapListArray removeObjectAtIndex:removeButton.tag];
    [self.swapListCollectionView reloadData];
    if(!swapListArray.count)
    {
        self.heightConstraintOfCollectionView.constant = 0;
    }
}
@end

