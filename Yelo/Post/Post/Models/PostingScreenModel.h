//
//  PostingScreenModel.h

//  Created by Rahul Sharma on 08/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UploadToCloudinary.h"
#import "Category.h"
#import "SubCategory.h"
#import "ProductDetails.h"

@interface PostingScreenModel : NSObject{
    NSMutableArray *arrayOfWidths,*arrayOfHeights;
    NSString *mainImgHeight,*height1,*height2,*height3,*height4;
    NSString *mainImgWidth,*width1,*width2,*width3,*width4;
}
@property(nonatomic,readonly)NSString *titleOfPost;
@property(nonatomic,readonly)NSString *descriptionForPost;
@property(nonatomic,readonly)NSString *category;
@property(nonatomic,readonly)NSString *subCategory;
@property(nonatomic,readonly)NSString *currency;
@property(nonatomic,readonly)NSString *condition;
@property(nonatomic,readonly)NSString *price;
@property(nonatomic,readonly)NSString *negotiable;
@property(nonatomic,readonly)NSString *place;
@property(nonatomic,readonly)NSString *latitude;
@property(nonatomic,readonly)NSString *longitude;
@property(nonatomic,readonly)NSString *postedImagePath;
@property(nonatomic,readonly)NSString *postedThumbnailPath;
@property(nonatomic)BOOL facebookSwitchState;
@property(nonatomic)BOOL twitterSwitchState;
@property(nonatomic)BOOL instgramSwitchState;
@property(nonatomic,readonly)NSArray *arrayOfImagePaths;
@property(nonatomic,readonly)NSString *tagPoductCoordinates;
@property(nonatomic,readonly)NSString *taggedFriendsString;
@property(nonatomic,readonly)NSString *postID;
@property (nonatomic)NSString *countryShortName;
@property (nonatomic)NSString *cityName;
@property(nonatomic)BOOL startUpload;
@property(nonatomic)BOOL editPost;
@property(nonatomic,readonly)NSString *createFilterJSONString;
@property(nonatomic,readonly) NSString *categoryId;
@property(nonatomic,readonly) NSString *categoryNodeId;

@property(nonatomic,copy) NSString *values;
@property(nonatomic,copy) NSString *subId;
@property(nonatomic,copy) NSString *fieldName;


@property(nonatomic,readonly) NSString *languageCode;
@property(nonatomic,readonly) NSString *subCategoryNodeId;


//EDIT

@property(nonatomic,readonly) NSString *editCategoryNodeId;
@property(nonatomic,readonly) NSString *editSubCategoryNodeId;


// Filters
@property(nonatomic,readonly)NSMutableArray *swapListArray;
@property(nonatomic,readonly)NSString *isSwap,*swapDescription, *createSwapArrayJSONString;

@property(weak,nonatomic) NSString *mainImgUrl, *imgUrl1,*imgUrl2,*imgUrl3,*imgUrl4;
@property(weak,nonatomic) NSString *mainthumbNailUrl,*thumbUrl1,*thumbUrl2,*thumbUrl3,*thumbUrl4;
@property(weak,nonatomic) NSString *cloudinaryPublicId,*cloudinaryPublicId1,*cloudinaryPublicId2,*cloudinaryPublicId3,*cloudinaryPublicId4;

@property (nonatomic, strong) PostingScreenModel *modelObj;
@property  (nonatomic,strong) SubCategory *subCat;

+(instancetype)sharedInstance;
-(id)initWithListings :(Listings *)listing;
-(void)getHeightsAndWidthsForContainer :(NSArray *)arrayOfImagePaths andCheckType:(BOOL)editPostType;
-(void)getHeightForParticularImage;
-(void)getWidthForParticularImage;
-(void)getImagesMainUrlsInStringFormat:(NSString *)mainImgUrl imgUrl1:(NSString *)imgUrl1 imgUrl2:(NSString *)imgUrl2 imgUrl3:(NSString *)imgUrl3 imgUrl4:(NSString *)imgUrl4 andThumnailUrls :(NSString *)mainthumbNailUrl thumbUrl1:(NSString *)thumbUrl1 thumbUrl2:(NSString *)thumbUrl2 thumbUrl3:(NSString *)thumbUrl3 thumbUrl4:(NSString *)thumbUrl4;
-(NSDictionary *)createParamDictionaryForAPICall : (PostingScreenModel *)postingScreenObject;
-(void)getUrlsOfImagePaths :(UploadToCloudinary *)cloudinaryObj;


@end
