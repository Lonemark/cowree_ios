
//  ProductImageTableViewCell.m
//  InstaVideoPlayerExample
//  Created by Rahul Sharma on 22/07/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.

#import "ProductImageTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "FontDetailsClass.h"
#import "ProfileViewController.h"

@interface ProductImageTableViewCell ()

@end

@implementation ProductImageTableViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.imageViewOutlet.image = nil ;
}

-(void)setProductImage :(ProductDetails *)product
{
    if(product.isSwap){
        self.swapIcon.hidden = NO ;
    }
    else{
        self.swapIcon.hidden = YES;
    }
//    NSString *productMainUrl =  [product.mainUrl stringByReplacingOccurrencesOfString:@"upload/" withString:@"upload/c_fit,h_500,q_40,w_500/"];
    
    if([[SDWebImageManager sharedManager] diskImageExistsForURL:[NSURL URLWithString:product.mainUrl]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.imageViewOutlet setImage: [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:product.mainUrl]];
        });
    }
    else
    {
        self.imageViewOutlet.alpha = 0 ;
        [self.imageViewOutlet setImageWithURL:[NSURL URLWithString:product.mainUrl] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageProgressiveDownload completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType,NSURL *imageURL){
            dispatch_async(dispatch_get_main_queue(),^{
                self.imageViewOutlet.image = image;});
        } usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] ;
        [UIView transitionWithView:self.imageViewOutlet duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            self.imageViewOutlet.alpha = 1.0;
        } completion:NULL];
    }
}




@end
