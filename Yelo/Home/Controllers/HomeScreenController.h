//
//  HomeScreenController

//
//  Created by Rahul Sharma on 4/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterViewController.h"
#import "PostingScreenModel.h"
#import <CoreLocation/CLLocationManager.h>
#import "Category.h"
#import "FilterListings.h"
@import FBAudienceNetwork;


@interface HomeScreenController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate,filterDelegate,FBNativeAdDelegate>
{
    PostingScreenModel *post;
    NSString *categoryInStringFormat ;
    UIRefreshControl *refreshControl;
    NSMutableArray  *arrayOfcategory,*selectedFilters ,*productsArray;
    UIActivityIndicatorView  *avForCollectionView;
    UICollectionView *filteredCV;
    NSString  *minPrice,*maxPrice, *sortBy,*postedWithin;
    float  dist;
    UILabel *errorMessageLabelOutlet;
    CGPoint lastScrollContentOffset;
    GetCurrentLocation *getLocation;
    BOOL newPostAdded;
    int holdNativeAdsCount ;
}

#pragma mark -
#pragma mark - Non IB Properties-

@property (nonatomic,strong)FilterListings *filterListings;
@property (nonatomic,strong)Category *categoryObj;
@property (nonatomic, strong) CLLocationManager * locationManager;
@property (nonatomic)BOOL isFilter, flagForLocation, noPostsAreAvailable ,isScreenDidAppear;
@property (nonatomic)double currentLat, currentLong;
@property (nonatomic,strong) NSString *locationName;
@property int currentIndex,paging;
@property NSInteger cellDisplayedIndex , leadingConstraint;
@property NSString *postedImagePath, *postedthumbNailImagePath;
@property (nonatomic, retain) UIDocumentInteractionController *dic;
@property (nonatomic, strong) ZoomInteractiveTransition * transition;
@property(assign, nonatomic) CGFloat currentOffset;

#pragma mark -
#pragma mark - UIView Outlets-
@property (strong, nonatomic) IBOutlet UIView *emtyFiltersView;


#pragma mark -
#pragma mark - Label Outlets-
@property (strong, nonatomic) IBOutlet UILabel *labelNotificationsCount;
@property (strong, nonatomic) IBOutlet UILabel *emptyFilterTextLabel;

#pragma mark -
#pragma mark - Button Outlets-

@property (weak, nonatomic) IBOutlet UIView *sellStuffButtonView;
@property (strong, nonatomic) IBOutlet UIButton *filterButtonOutlet;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *rightBarButtonItem;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *searchButtonOutlet;


#pragma mark -
#pragma mark - CollectionView Outlets

@property (weak, nonatomic) IBOutlet UICollectionView *collectionviewFilterItem;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewOutlet;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewForCategory;
@property (strong, nonatomic) IBOutlet UILabel *navTitleLabel;



#pragma mark -
#pragma mark - Button Actions-
- (IBAction)searchButtonAction:(id)sender;
- (IBAction)notificationButtonACtion:(id)sender;
- (IBAction)filterButtonAction:(id)sender;
- (IBAction)sellStuffButton:(id)sender;
- (IBAction)removeProductFilterButtonAcion:(id)sender;


#pragma mark -
#pragma mark - NSConstraints Outlets

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraintOfSellButton;

- (IBAction)clearFiltersButtonAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *selectedFiltersView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *categoryFiltersViewHeight;



@end

