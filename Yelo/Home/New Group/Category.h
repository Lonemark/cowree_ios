//
//  Category.h
//  CollegeStax
//
//  Created by Rahul Sharma on 18/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Category : NSObject

@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *activeimage;
@property(nonatomic)NSUInteger filterCount;
@property(nonatomic)NSUInteger subCategoryCount;
@property(nonatomic,retain)NSMutableArray *filterArray;
@property(nonatomic,copy)NSString *categoryNodeId;
@property(nonatomic,copy)NSString *catId;

@property(nonatomic,copy)NSString *subCategoryNodeId;

- (instancetype)initWithDictionary:(NSDictionary *)response;
+(NSMutableArray *) arrayOfCategory :(NSArray *)responseData ;


@end
