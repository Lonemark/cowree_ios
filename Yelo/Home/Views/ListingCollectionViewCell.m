//
//  ListingCollectionViewCell.m

//
//  Created by Rahul Sharma on 4/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "ListingCollectionViewCell.h"
#import "TinderGenericUtility.h"
#import "UIImageView+WebCache.h"

@implementation ListingCollectionViewCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.layer.masksToBounds = NO ;
    self.layer.shadowColor = mDividerColor.CGColor;
    self.layer.shadowOffset = CGSizeMake(2.0f, 2.0f) ;
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = 2.0 ;
    self.layer.borderColor = mDividerColor.CGColor ;
    self.postedImageOutlet.contentMode = UIViewContentModeScaleAspectFill;
    [self layoutIfNeeded];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    self.postedImageOutlet.image = nil;
    self.postedImageOutlet.clipsToBounds = YES ;
    
}

/**
 Set Products on homescreen.

 @param product productModel.
 */
-(void)setProducts:(ProductDetails *)product;
{
    if(product.isSwap)
    {
        self.swapIcon.hidden = NO ;
        NSMutableArray *swapPostsName = [NSMutableArray new];
        for (PostSuggession *swapPost in product.swapPosts) {
            [swapPostsName addObject:swapPost.productName];
        }
        
        self.swapFor.text = [NSString stringWithFormat:@"Swap For : %@",[swapPostsName componentsJoinedByString:@", "]];
    }
    else
    {
        self.swapIcon.hidden = YES ;
        self.swapFor.text = @"";
    }
    
    self.productName.text = product.productName;
    self.price.text = [NSString stringWithFormat:@"%@ %@",[Helper returnCurrency: product.currency],product.price];

    if(product.isPromoted)
    {
        self.featuredView.hidden = NO ;
    }
    else
    {
        self.featuredView.hidden = YES ;
    }
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    
    [self.postedImageOutlet setImageWithURL:nil usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    // request image

     NSString *productMainUrl =  [product.mainUrl stringByReplacingOccurrencesOfString:@"upload/" withString:@"upload/c_fit,h_500,q_40,w_500/"];
    
    if([manager diskImageExistsForURL:[NSURL URLWithString:productMainUrl]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.postedImageOutlet setImage: [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:productMainUrl]];
        });
    }
    else
    {
        [self.postedImageOutlet setImageWithURL:[NSURL URLWithString:productMainUrl] placeholderImage:[UIImage imageNamed:@""] options:0 completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType,NSURL *imageURL){
            dispatch_async(dispatch_get_main_queue(),^{
                [UIView transitionWithView:self.postedImageOutlet duration:0.2 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                    [self.postedImageOutlet setImage:image];
                }completion:NULL];
            });
        } usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] ;
        
    }
    
}

@end
