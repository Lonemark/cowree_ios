//
//  CellForDistance.m

//  Created by Rahul Sharma on 12/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CellForDistance.h"

@implementation CellForDistance

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)sliderValueChangedAction:(id)sender {
    int val  = [[NSString stringWithFormat:@"%f",self.sliderForDistance.value]intValue];
    CGRect trackRect = [self.sliderForDistance trackRectForBounds:self.sliderForDistance.bounds];
    CGRect thumbRect = [self.sliderForDistance thumbRectForBounds:self.sliderForDistance.bounds
                                             trackRect:trackRect
                                                 value:self.sliderForDistance.value];
//    self.label.center = CGPointMake(thumbRect.origin.x + self.sliderForDistance.frame.origin.x + 10 , self.sliderForDistance.frame.origin.y - 10);
    self.labelLeadingConstraint.constant = thumbRect.origin.x + self.sliderForDistance.frame.origin.x - 10;
    self.leadingConstraint = self.labelLeadingConstraint.constant ;
    [self setDistance:val];
}

-(void)setValuesForSlider:(int)distanceValue andLeadingConstraint :(NSInteger)leadingConstraint
{
    self.sliderForDistance.value = distanceValue;
    self.labelLeadingConstraint.constant = leadingConstraint;
    [self setDistance:distanceValue];

}

-(void)setDistance:(int)distanceValue
{
    if(distanceValue == 0)
    {
        if(self.callBackForDistance)
        {
            self.callBackForDistance(0 ,self.labelLeadingConstraint.constant);
        }
        self.label.text = NSLocalizedString(distanceFilterNotSet,distanceFilterNotSet ) ;
    }
    else if(distanceValue <=30 )
    {
        if(self.callBackForDistance)
        {
            self.callBackForDistance(distanceValue , self.leadingConstraint);
        }
        self.label.text = [[NSString stringWithFormat:@"%d ",distanceValue]stringByAppendingString:@"Km"];
    }
    
    else
    {
        if(self.callBackForDistance)
        {
            self.callBackForDistance(3000 , self.leadingConstraint);
        }
        self.label.text = NSLocalizedString(distanceFilterMax,distanceFilterMax);
    }

}

@end
