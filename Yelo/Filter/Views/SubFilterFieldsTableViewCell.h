//
//  SubFilterFieldsTableViewCell.h
//  CollegeStax
//
//  Created by 3Embed on 22/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Filter.h"
@interface SubFilterFieldsTableViewCell : UITableViewCell <UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate>
{
    NSArray *filterValues;
    NSString *textBoxValue, *fromRangeValue, *toRangeValue;
}

@property (weak, nonatomic) IBOutlet UILabel *subFilterFieldsName;
@property (weak, nonatomic) IBOutlet UITextField *textBoxTextField;
@property (weak, nonatomic) IBOutlet UITextField *rangeFromTextfield;
@property (weak, nonatomic) IBOutlet UITextField *rangeToTextField;

@property(strong,nonatomic)FilterListings *filterListingObj;
@property (weak, nonatomic) IBOutlet UIView *textBoxView;
@property (weak, nonatomic) IBOutlet UIView *rangeView;

@property (weak, nonatomic) IBOutlet UIView *sliderView;
@property (weak, nonatomic) IBOutlet UITableView *listTableView;

-(void)setFilterFieldsFor :(Filter *)filterFields ;

- (IBAction)textFieldValueEditingChanged:(id)sender;

@end
