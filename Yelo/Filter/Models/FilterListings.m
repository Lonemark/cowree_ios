//
//  FilterListings.m
//  CollegeStax
//
//  Created by 3Embed on 22/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "FilterListings.h"

@implementation FilterListings


-(instancetype)init
{
    self = [super init];
    if (!self) { return nil; }
    
    self.category = @"";
    self.subcategoryNodeId = @"";
    self.subcategoryId = @"";
    
    
    self.subcategoryName = @"";
    self.categoryImage = @"";
    self.subcategoryImage = @"";
    self.sortBy = @"";
    self.postedWithin = @"";
    
    self.currencyCode = [NSString stringWithFormat:@"EUR"];
    self.currencySymbol = [NSString stringWithFormat:@"EUR"];
    self.price = @"";
    self.from = @"";
    self.to = @"";
    self.distanceFilter = @"";
    self.distanceMax = @"";
    self.leadingConstraint = 40;
    self.location = @"";
    self.latitude = @"";
    self.longitude = @"";
    self.LanguageCode = [Helper currentLanguage];
    self.advanceFilterCellHeight = 0;
    self.pagingIndex = 0;
    self.subFilterKeyValue = [NSMutableArray new];
    self.filtersDictionary = [NSMutableDictionary new];
    return self;
}


-(NSMutableArray *)getFilterArray
{
    NSMutableArray *filterList = [NSMutableArray new];
    
    if(self.filtersDictionary[@"Category"])
    {
        NSMutableDictionary  *category = [[NSMutableDictionary alloc] init];
        [category setValue:@"category" forKey:@"typeOfFilter"];
        [category setValue:self.category forKey:@"value"];
        [category setValue:self.categoryImage forKey:@"image"];
        [filterList addObject:category];
    }
    
    if(self.filtersDictionary[@"Subcategory"])
    {
        NSMutableDictionary  *subcategory = [[NSMutableDictionary alloc] init];
        [subcategory setValue:@"subcategory" forKey:@"typeOfFilter"];
        [subcategory setValue:self.subcategoryName forKey:@"value"];
        [subcategory setValue:self.subcategoryImage forKey:@"image"];
        [filterList addObject:subcategory];
    }
    
    if(self.subFilterKeyValue.count)
    {
        
        for(NSDictionary *keyValue in self.subFilterKeyValue)
        {
            
            if([keyValue[@"type"] isEqualToString:@"range"])
            {
                NSMutableDictionary  *subFilters = [[NSMutableDictionary alloc] init];
                [subFilters setValue:@"subFilters" forKey:@"typeOfFilter"];
                [subFilters setValue:keyValue[@"value"] forKey:@"value"];
                [filterList addObject:subFilters];
            }
            else
            {
                
                NSMutableDictionary  *subFilters = [[NSMutableDictionary alloc] init];
                [subFilters setValue:@"subFilters" forKey:@"typeOfFilter"];
                [subFilters setValue:keyValue[@"value"] forKey:@"value"];
                [filterList addObject:subFilters];
            }
        }
    }
    
    if(self.sortBy.length)
    {
        NSMutableDictionary  *sortBy = [[NSMutableDictionary alloc] init];
        [sortBy setValue:@"sortBy" forKey:@"typeOfFilter"];
        [sortBy setValue:self.sortBy forKey:@"value"];
        [filterList addObject:sortBy];
    }
    
    if(self.postedWithin.length)
    {
        NSMutableDictionary  *postedWithin = [[NSMutableDictionary alloc] init];
        [postedWithin setValue:@"postedWithin" forKey:@"typeOfFilter"];
        [postedWithin setValue:self.postedWithin forKey:@"value"];
        [filterList addObject:postedWithin];
    }
    
    if(self.price.length)
    {
        NSMutableDictionary  *priceDic = [[NSMutableDictionary alloc] init];
        [priceDic setValue:@"price" forKey:@"typeOfFilter"];
        [priceDic setValue:self.price forKey:@"value"];
        [filterList addObject:priceDic];
    }
    
    if(![self.distanceFilter isEqualToString:@"0 Km"] && self.distanceFilter.length)
    {
        NSMutableDictionary  *distanceFilter = [[NSMutableDictionary alloc] init];
        [distanceFilter setValue:@"distance" forKey:@"typeOfFilter"];
        [distanceFilter setValue:self.distanceFilter forKey:@"value"];
        [filterList addObject:distanceFilter];
    }
    
    if(self.location.length)
    {
        NSMutableDictionary  *locationFilter = [[NSMutableDictionary alloc] init];
        [locationFilter setValue:@"location" forKey:@"typeOfFilter"];
        [locationFilter setValue:self.location forKey:@"value"];
        [filterList addObject:locationFilter];
    }
    return filterList;
}



/**
 This method returns the dictonary of applied filters as
 parameters.
 @return param dictionary
 */
-(NSDictionary *)getParamDictionary
{
    
    
    NSString *loc ,*lat ,*longi;
    
    if(self.location.length)
    {
        loc = self.location;
        lat = self.latitude;
        longi = self.longitude;
    }
    else
    {
        loc = self.defaultLocation;
        lat = self.defaultLatitude;
        longi = self.defaultLongitude;
    }
    
    
    NSDictionary *requestDic = @{@"token":[Helper userToken],
                                 mLanguageCode : [Helper currentLanguage],
                                 @"location":flStrForObj(loc),
                                 @"latitude":flStrForObj(lat),
                                 @"longitude":longi,
                                 @"distanceMax":flStrForObj(self.distanceMax),
                                 @"category":flStrForObj(self.category),
                                 @"categoryId":flStrForObj(self.categoryNodeId),
                                 //  @"subCategoryId":flStrForObj(self.subcategoryNodeId),
                                 @"subCategory":flStrForObj(self.subcategoryName),
                                 @"subCategoryId":flStrForObj(self.subcategory.subCategoryNodeId),
                                 @"filter":[self getEncodedSubFilters],
                                 @"sortBy":flStrForObj([self sortByFilterValue]),
                                 @"postedWithin":[self postedWithInFilterValue],
                                 @"price":[self getPriceEncodeString],
                                 @"offset":flStrForObj([NSNumber numberWithInteger:self.pagingIndex * 20]),
                                 @"limit" : @"20"
                                 
                                 };
    return requestDic;
}

-(NSString *)sortByFilterValue
{
    NSString *sortBy = @"";
    
    if([self.sortBy isEqualToString:sortingByNewestFirst])
    {
        sortBy = @"newFirst";
    }
    else if ([self.sortBy isEqualToString:sortingByClosestFirst])
    {
        sortBy = @"closestFirst";
    }
    else if ([self.sortBy isEqualToString:sortingByPriceLowToHigh])
    {
        sortBy = @"lowToHigh";
    }
    else if ([self.sortBy isEqualToString:sortingByPriceHighToLow])
    {
        sortBy = @"heighToLow";
    }
    
    return sortBy;
}


-(NSString *)postedWithInFilterValue
{
    NSString *postedWithIn = @"";
    
    if([self.postedWithin isEqualToString:postedWithLast24h])
    {
        postedWithIn = @"1";
    }
    else if ([self.sortBy isEqualToString:sortingByClosestFirst])
    {
        postedWithIn = @"2";
    }
    else if ([self.sortBy isEqualToString:postedWithLast30d])
    {
        postedWithIn = @"3";
    }
    
    return postedWithIn;
}

-(NSString *)getPriceEncodeString
{
    
    NSDictionary *priceDict = @{@"currency":self.currencyCode,
                                @"from":self.from,
                                @"to":self.to
                                };
    NSError *error = nil;
    NSString *priceString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:priceDict
                                                                                           options:NSJSONWritingPrettyPrinted
                                                                                             error:&error]
                                                  encoding:NSUTF8StringEncoding];
    
    return priceString;
}


-(NSString *)getEncodedSubFilters
{
    NSError *error = nil;
    NSString *subFilterString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:self.subFilterKeyValue
                                                                                               options:NSJSONWritingPrettyPrinted
                                                                                                 error:&error]
                                                      encoding:NSUTF8StringEncoding];
    
    return subFilterString;
}

-(void)resetFilterListings
{
    self.category = @"";
    self.categoryNodeId = @"";
    self.subcategoryNodeId = @"";
    
    self.subcategoryName = @"";
    [self.filtersDictionary removeAllObjects];
    [self.subFilterKeyValue removeAllObjects];
    self.subcategoryArray = nil ;
    self.subcategory = nil;
    self.filtersArray = nil;
    self.advanceFilterCellHeight = 0;
    self.sortBy = @"";
    self.postedWithin = @"";
    self.price = @"";
    self.currencyCode = @"EUR";
    self.currencySymbol = @"EUR";
    self.location = @"";
    self.distanceFilter = @"";
    self.distanceMax = @"";
    self.leadingConstraint = 40;
    self.from = @"";
    self.to = @"";
    
    
}

@end
