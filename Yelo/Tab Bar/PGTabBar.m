
//
//  TabBar.m

//
//  Created by Rahul Sharma on 2/26/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "PGTabBar.h"
#import "Helper.h"
#import "ProfileViewController.h"
#import "CommonMethods.h"
#import "CameraViewController.h"
@interface PGTabBar ()<UITabBarDelegate,UITabBarControllerDelegate>

@end
#define kTabBarHeight = 40;
@implementation PGTabBar


+(instancetype)sharedInstance
{
    static PGTabBar *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[PGTabBar alloc] init];
    });
    return sharedInstance;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    self.tabBarItem.title = @"";
    
    // [self.tabBarController setSelectedIndex:1];
    // To show login screen only if user logoutrecently.
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"Didot-Bold" size:22.0f],
                                                        NSForegroundColorAttributeName :[UIColor blackColor]
                                                        } forState:UIControlStateSelected];
    
//    [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:0.9765 green:0.9765 blue:0.9765 alpha:1.0]];
   
}

- (void)viewWillLayoutSubviews {
//    CGRect tabFrame = self.tabBar.frame; //self.TabBar is IBOutlet of your TabBar
    // if the device is 6s/6s plus or 6/6plus tabbar height is 50 and for remining device tabbar height is 40.
//    if (CGRectGetHeight(self.view.frame) == 736 || CGRectGetHeight(self.view.frame) == 667 ) {
//        tabFrame.size.height = 50;
//        tabFrame.origin.y = self.view.frame.size.height - 50;
//    }
//    else {
//        tabFrame.size.height = 40;
//        tabFrame.origin.y = self.view.frame.size.height - 40;
//    }
//    self.tabBar.frame = tabFrame;

}
/* ------------------------------*/
#pragma mark
#pragma mark - tab bar buttons
/* ------------------------------*/

/*
 *  delegate method called when user taps on tab bar button.
 */
-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    
    // tag for home --- 1
    // tag for search  --- 2
    // tag for camera -- 3
    // tag for activity -- 4
    // tag for user profile -- 5
    
    if ([[Helper userToken] isEqualToString:@"guestUser"]) {
        [self.tabBarController setSelectedIndex:0];
    }
    else
        
    {
        switch (item.tag) {
            case 1:
                [[[self viewControllers] objectAtIndex:0] popToRootViewControllerAnimated: NO];
                [self.tabBarController setSelectedIndex:1];
                break;
            case 3:{
                if([[Helper userToken]isEqualToString:mGuestToken])
                {
                    UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
                    [self presentViewController:navigationController animated:YES completion:nil];

                 }
               }
                break;
            case 5:{
                ProfileViewController *vc = [[ProfileViewController alloc] init];
                vc.isMemberProfile = NO;
            }
                
            default:
                break;
        }
    }
}

- (BOOL) tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    NSUInteger indexOfTab = [tabBarController.viewControllers indexOfObject:viewController];
    if ([[Helper userToken] isEqualToString:@"guestUser"] && indexOfTab!=0) {
        UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
        [self presentViewController:navigationController animated:YES completion:nil];
        return NO;
    }
    else {
        return YES;
    }
}





-(void)selectHomeScreen {
      [self.tabBarController setSelectedIndex:0];
}


- (BOOL)isTabBarHidden {
    CGRect viewFrame = self.view.frame;
    CGRect tabBarFrame = self.tabBar.frame;
    return tabBarFrame.origin.y >= (viewFrame.size.height +49);
}


- (void)setTabBarHidden:(BOOL)hidden {
    [self setTabBarHidden:hidden animated:NO];
}


- (void)setTabBarHidden:(BOOL)hidden animated:(BOOL)animated {
    BOOL isHidden = self.tabBarHidden;
    if(hidden == isHidden)
        return;
    UIView *transitionView = [[[self.view.subviews reverseObjectEnumerator] allObjects] lastObject];
    if(transitionView == nil) {
        NSLog(@"could not get the container view!");
        return;
    }
    CGRect viewFrame = self.view.frame;
    CGRect tabBarFrame = self.tabBar.frame;
    CGRect containerFrame = transitionView.frame;
    tabBarFrame.origin.y = viewFrame.size.height - (hidden ? 0 : tabBarFrame.size.height);
    containerFrame.size.height = viewFrame.size.height - (hidden ? 0 : tabBarFrame.size.height);
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.tabBar.frame = tabBarFrame;
                         transitionView.frame = containerFrame;
                     }
     ];
}


@end
