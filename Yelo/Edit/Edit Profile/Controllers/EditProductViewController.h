//
//  EditProductViewController.h

//
//  Created by Rahul Sharma on 01/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetails.h"


@interface EditProductViewController : UIViewController<WebServiceHandlerDelegate>

@property (strong, nonatomic)ProductDetails *product ;
@property (nonatomic) BOOL showInsights ;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong,nonatomic) NSDictionary *memberPostDiction;

/**
 UI Outlets.
 */
@property (weak, nonatomic) IBOutlet UIView *markAsSoldView;

@property (weak, nonatomic) IBOutlet UIView *updateView;

@property (strong, nonatomic) IBOutlet UILabel *navTitleLabel;

@property (strong, nonatomic) IBOutlet UIImageView *navTitleImageView;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewItemImage;
@property (strong, nonatomic) IBOutlet UILabel *labelProductName;
@property (strong, nonatomic) IBOutlet UILabel *labelCategory;

@property NSString *postImagePath;
@property ( nonatomic) NSDictionary *commingFromChat;
@property (strong, nonatomic) IBOutlet UIButton *promoteThisItem;

/**
    Buttons Action.
 */
- (IBAction)buttonMarkAsAction:(id)sender;
- (IBAction)buttonEditAction:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *buttonShowItemAction;
@property (strong, nonatomic) IBOutlet UIView *backViewForNoOffers;
- (IBAction)showFullProductImageButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *insightsButtonOutlet;
- (IBAction)promotoButtonAction:(id)sender;

@end
