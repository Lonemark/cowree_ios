//
//  InsightsTableviewCell
//  Yelo
//
//  Created by Rahul Sharma on 02/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InsightsTableviewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *countryNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *clicksLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imageviewForArrow;
@property (strong, nonatomic) IBOutlet UILabel *cityNameLabel;

@end
