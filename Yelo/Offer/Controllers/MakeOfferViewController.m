//
//  MakeOfferViewController.m

//
//  Created by Rahul Sharma on 10/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "MakeOfferViewController.h"
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"
#import "ProgressIndicator.h"
#import "Helper.h"
#import "AppDelegate.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <QuartzCore/QuartzCore.h>
#import "CouchbaseLite/CouchbaseLite.h"
#import "CBObjects.h"
#import "CouchbaseEvents.h"
#import "ContacDataBase.h"
#import "HomeScreenController.h"
#import "Cowree-Swift.h"

@interface MakeOfferViewController ()<UITextFieldDelegate>


@end

@implementation MakeOfferViewController
{
    NSMutableArray *selectedUsers;
    NSString *currency;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
    
    self.OfferpriceTextField.delegate = self;
    
    self.makeOfferOutlet.layer.cornerRadius = 24;
    
    //  KeyboardWillShowNotification.
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasHidden:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    //when keyboard appears this will  notifiy.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [self setValuesForMakeOffer:self.product];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    UIButton *navCancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName]forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName] forState:UIControlStateSelected];
    [navCancelButton addTarget:self action:@selector(backButtonClicked)  forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0,0,40,40)];
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = -14;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    self.navigationController.navigationBar.translucent = NO ;
    self.navigationController.navigationBar.shadowImage = nil;
}

-(void)setValuesForMakeOffer:(ProductDetails *)product {
    selectedUsers = [[NSMutableArray alloc]init];
    NSString *user = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"]];
    
    NSString *ch = [NSString stringWithFormat:@"%@",product.memberId];
    if([ch isEqualToString:@"(null)"]) {
        return ;
    }
    if(![user isEqualToString:ch])
        [selectedUsers addObject:product.memberId];
    else
        [selectedUsers addObject:product.userId];
    
    self.navigationItem.title = NSLocalizedString(navTitleForMakeOffer, navTitleForMakeOffer);
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-60, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    NSString *imageUrl = [NSString stringWithFormat:@"%@",product.thumbnailImageUrl];
    [self.productImage setImageWithURL:[NSURL URLWithString:imageUrl] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    currency = [self returnCurrency:[NSString stringWithFormat:@"%@",product.currency]];
    self.productName.text = product.productName;
   
    NSString * city = [product.city capitalizedString];
    NSString *countryName = [[NSLocale systemLocale] displayNameForKey:NSLocaleCountryCode value:product.countrySname];
    self.productArea.text = [NSString stringWithFormat:@"%@, %@",city , countryName];
    
    self.ProductDistance.text = @"";
    self.actualPrice.text = [NSString stringWithFormat:@"%@ %@",currency,product.price];
    
    self.OfferpriceTextField.text = [NSString stringWithFormat:@"%@ %@",currency,product.price];
    if(!self.product.negotiable)
    {
        self.OfferpriceTextField.enabled = NO ;
    }
    if(self.product.negotiable|| ([product.membername isEqualToString:[Helper userName]]))
    {
        self.OfferpriceTextField.enabled = YES;
    }
}

-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    
}
- (void)keyboardWasHidden:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    int height = MIN(keyboardSize.height,keyboardSize.width);
    //    int width = MAX(keyboardSize.height,keyboardSize.width);
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.viewbuttomConstrain.constant = height;
                         [self.view layoutIfNeeded];
                     }];
}
- (void)keyboardWasShown:(NSNotification *)notification
{
    
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    int height = MIN(keyboardSize.height,keyboardSize.width);
    self.viewbuttomConstrain.constant = height;
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.viewbuttomConstrain.constant = 0;
                         [self.view layoutIfNeeded];
                     }];
}

// get currency
-(NSString*)returnCurrency:(NSString *)currencyCon
{
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:currencyCon];
    NSString *currencySymbol = [NSString stringWithFormat:@"%@",[locale displayNameForKey:NSLocaleCurrencySymbol value:currencyCon]];
    NSLog(@"Currency Symbol : %@", currencySymbol);
    return currencySymbol;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Text Field Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"");
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *curr = [NSString stringWithFormat:@"%@ ",currency];

    _makeOfferOutlet.enabled = YES;
    _makeOfferOutlet.alpha = 1.0f;
    
    if ([textField.text isEqualToString:curr] && [string length]== 0) {
        _makeOfferOutlet.enabled = NO;
        _makeOfferOutlet.alpha = 0.5f;
        return NO;
    }
    return [Helper textFieldLimitAfterDot:self.OfferpriceTextField newString:string forRange:range];
    return YES;
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self.OfferpriceTextField resignFirstResponder];
}

- (IBAction)makeOfferButtonAction:(id)sender {

    NSString *curr = [NSString stringWithFormat:@"%@ ",currency];
    NSString *price = [self.OfferpriceTextField.text stringByReplacingOccurrencesOfString:curr withString:@""];
    [self createObjectForMakeOfferwithData:self.product withPrice:price andType:@"15" withAPICall:YES];
}

-(void)createObjectForMakeOfferwithData:(ProductDetails *)product withPrice:(NSString *)price andType:(NSString *)type withAPICall:(BOOL)isAPICall {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    IndividualChatViewModel *individualCVMObj= [[IndividualChatViewModel alloc] initWithCouchbase:[Couchbase sharedInstance]];
    
    NSString *receiverID = product.memberMqttId;
    NSString *secretID = product.postId;
    NSString *toDocID = [individualCVMObj fetchIndividualChatDocWithRecieverID:receiverID andSecretID:secretID];
    
    if (toDocID.length>0) {
        data[@"offerStatus"] = @"3";
        NSDictionary *messageObject = [self createObjectForMessageWithDocID:toDocID withPrice:price andType:type andProduct:product];
        data[@"sendchat"] = messageObject;
    }else {
        data[@"offerStatus"] = @"1";
        NSDictionary *messageObject = [self createObjectForMessageWithDocID:@" " withPrice:price andType:type andProduct:product];
        data[@"sendchat"] = messageObject;
    }
    
    data[@"token"] = [Helper userToken];
    data[@"postId"] = product.postId;
    data[@"price"] =  price;
    data[@"type"] = @"0";
    data[@"membername"] = product.membername ;
    
    ProgressIndicator *progress = [ProgressIndicator sharedInstance];
    [progress showPIOnView:self.view withMessage:@"Loading.."];
    
    // API call for initiating offer
    if (isAPICall) {
        API *apiObj = [[API alloc] init];
        NSDictionary *params = @{@"receiverID":receiverID,
                                @"secretID":secretID};
        
        [apiObj sendNotificationWithText:@" " andTitle:@"You got an Offer" toTopic:receiverID andData:params];
        [apiObj initiatedChatWithWithdata:data completionBlock:^(NSDictionary<NSString *,id> * _Nullable response) {
            if (response) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self goToChatControllerWithRecieverID:receiverID andSecretID:secretID andProductbj:product];
                    [[ProgressIndicator sharedInstance] hideProgressIndicator];
                });
            }
        }];
    } else {
        [self goToChatControllerWithRecieverID:receiverID andSecretID:secretID andProductbj:product];
    }
}

-(void)goToChatControllerWithRecieverID:(NSString *)receiverID andSecretID : (NSString *)secretID andProductbj : (ProductDetails *)productObj {
    
    NSMutableDictionary *chatObj = [NSMutableDictionary new];
    chatObj[@"receiverID"] = receiverID;
    chatObj[@"secretID"] = secretID;
    
    // redirecting it to the next controller with the chat object data.
    // check for empty object.
    UIViewController *tabController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    if ([tabController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tbControler = (UITabBarController *)tabController;
        dispatch_async(dispatch_get_main_queue(), ^{
            [tbControler setSelectedIndex:3];
            [self.navigationController dismissViewControllerAnimated:false completion:nil];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"OfferInitiated" object:nil userInfo:@{@"chatObj":chatObj,@"productObj":productObj}];
            });
        });
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tabBarController setSelectedIndex:3];
        [self.navigationController dismissViewControllerAnimated:false completion:nil];

    });
}

-(NSDictionary *)createObjectForMessageWithDocID : (NSString *) docID withPrice:(NSString *)price andType:(NSString *)type andProduct :(ProductDetails *)product {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    NSData *ploadData = [price dataUsingEncoding:NSUTF8StringEncoding];
    
    // Get NSString from NSData object in Base64
    NSString *base64Encoded = [ploadData base64EncodedStringWithOptions:0];
    if ([price isEqualToString:@"0"]){
        base64Encoded = @"";
    }
    UInt64 timeStamp = ([[NSDate date] timeIntervalSince1970] * 1000);
    
    data[@"name"] = [Helper userName];
    data[@"from"] = [Helper getMQTTID];
    data[@"to"] = product.memberMqttId ; ; // have to check about the other persons mqtt id
    data[@"payload"] = base64Encoded; //
    data[@"type"] = type;
    data[@"offerType"] = @"1"; // offertype 1: make offer 3:counter offer 2: accepted
    data[@"id"] = [NSString stringWithFormat:@"%llu",timeStamp];
    data[@"secretId"] = [NSString stringWithFormat:@"%@",product.postId];
    data[@"thumbnail"] = @"";
    data[@"userImage"] = product.profilePicUrl;
    data[@"toDocId"] = [NSString stringWithFormat:@"%@",docID];
    data[@"dataSize"] = [NSNumber numberWithInt:1];
    data[@"isSold"] = @"0";
    data[@"productImage"] = product.thumbnailImageUrl ;
    data[@"productId"] = [NSString stringWithFormat:@"%@",product.postId];
    data[@"productName"] = product.productName;
    data[@"productPrice"] = price;
    return data;
}

#pragma mark - Push Back Button

- (IBAction)backToProductAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end

