//
//  PromotionsPlanTableViewCell.h
//  Yelo
//
//  Created by Rahul Sharma on 19/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromotionsPlanTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *viewForPlans;
@property (strong, nonatomic) IBOutlet UILabel *numberOfLikes;
@property (strong, nonatomic) IBOutlet UILabel *priceOfPlan;

-(void)setViewsWithData:(NSArray *)arrayOfPlans forIndex : (NSInteger)index;

-(void)updateViewForActiveState :(BOOL)active;


@end
