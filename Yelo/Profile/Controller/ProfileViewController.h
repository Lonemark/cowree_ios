//
//  ProfileViewController.h
//  Tac Traderz
//
//  Created by 3Embed on 16/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Profile.h"
#import "SVPullToRefresh.h"

@interface ProfileViewController : UIViewController <GIDSignInDelegate , GIDSignInUIDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableViewOutlet;

- (IBAction)addPeopleButtonAction:(id)sender;

@property bool  isMemberProfile, productDetails;
@property (strong,nonatomic) NSString *memberName;
@property(strong,nonatomic)Profile *profile;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIImageView *headerProfileImage;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;

@property (weak, nonatomic) IBOutlet UIButton *discoverPeopleButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *settingsButtonOutlet;

@property (weak, nonatomic) IBOutlet UIButton *editProfileButtonOutlet;

- (IBAction)settingsButtonAction:(id)sender;

- (IBAction)editButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewTopConstraint;


@property (strong, nonatomic) IBOutlet UIView *backgroundViewForNoPosts;



- (IBAction)startSellingButtonAction:(id)sender;

@property(retain,nonatomic) UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet UIButton *startSellingButton;
@property (weak, nonatomic) IBOutlet UILabel *noPostsDescriptionLabel;

@property (weak, nonatomic) IBOutlet UILabel *noPostsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *noPostsEmptyImage;
@property (weak, nonatomic) IBOutlet UIButton *startDiscoveringButton;

- (IBAction)startDiscoverButtonAction:(id)sender;

-(void)presentLoginForGuestUsers;


@end
