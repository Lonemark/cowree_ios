//
//  ExchangePost.m
//  Tac Traderz
//
//  Created by 3Embed on 31/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "Exchanged.h"

@implementation Exchanged



/**
 Initialization Method to return the object of Exchanged Posts Model.
 
 @param response response dictionary contains proerties of product.
 
 @return self object of model.
 */
-(instancetype)initWithDictionary:(NSDictionary *)response
{
    self = [super init];
    if (!self) { return nil; }
    
    self.acceptedTime = flStrForObj(response[@"acceptedOn"]);
    self.mainUrl    = flStrForObj(response[@"mainUrl"]);
    self.productName    = flStrForObj(response[@"productName"]);
    self.postId    = flStrForObj(response[@"postId"]);
    
    self.swapPostId    = flStrForObj(response[@"swapPostId"]);
    self.swapMainUrl    = flStrForObj(response[@"swapMainUrl"]);
    self.swapProductName    = flStrForObj(response[@"swapProductName"]);

    self.thumbnailImageUrl     = flStrForObj(response[@"thumbnailImageUrl"]);
    self.swapThumbnailImageUrl    = flStrForObj(response[@"swapThumbnailImageUrl"]);
    
    return self;
}


+(NSMutableArray *) arrayOfExchangedPosts :(NSArray *)responseData
{
    NSMutableArray *exhangedArray = [[NSMutableArray alloc]init];
    for(NSDictionary *productDict in responseData) {
        
        Exchanged *product = [[Exchanged alloc]initWithDictionary:productDict] ;
        [exhangedArray addObject:product];
    }
    
    return exhangedArray ;
}
@end
