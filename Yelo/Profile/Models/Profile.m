//
//  Profile.m
//  Tac Traderz
//
//  Created by 3Embed on 16/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "Profile.h"

@implementation Profile

-(instancetype)init
{
    self = [super init];
    if (!self) { return nil; }

    self.sellingPostData = [NSMutableArray new];
    self.soldPostData = [NSMutableArray new];
    self.likesPostData = [NSMutableArray new];
    self.exchangePostData = [NSMutableArray new];
    self.memberName = @"";
    self.profileDetails = [[ProfileDetails alloc]init];
    self.selectedButtonIndex = 5;
    return self;
}


@end
