//
//  ProductDetails.m
//  Trgovina
//
//  Created by Rahul Sharma on 02/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "ProductDetails.h"

@implementation ProductDetails


/**
 Initialization Method to return the object of ProductDetails Model.
 
 @param response response dictionary contains proerties of product.
 
 @return self object of model.
 */
-(instancetype)initWithDictionary:(NSDictionary *)response
{
    self = [super init];
    if (!self) { return nil; }
    
    self.code = [flStrForObj(response[@"code"]) integerValue];
    self.productName = flStrForObj(response[@"productName"]);
    self.subCategoryNodeId = flStrForObj(response[@"subCategoryNodeId"]);
    
    if(self.productName.length){
        self.productName = [self.productName stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[self.productName substringToIndex:1] uppercaseString]];
    }
    
    self.category = flStrForObj(response[@"categoryData"][0][@"category"]);
    
    if(self.category.length < 1)
    {
        self.category = flStrForObj(response[@"category"]);
    }
    
    self.subcategory = flStrForObj(response[@"subCategory"]);
    self.categoryNodeId = flStrForObj(response[@"categoryNodeId"]);
    self.clickCount = flStrForObj(response[@"clickCount"]);
    self.condition = flStrForObj(response[@"condition"]);
    self.currency = flStrForObj(response[@"currency"]);
    self.price = flStrForObj(response[@"price"]);
    self.productDescription = flStrForObj(response[@"description"]);
    self.followRequestStatus = [flStrForObj(response[@"followRequestStatus"]) boolValue];
    self.mainUrl = flStrForObj(response[@"mainUrl"]);
    self.imageUrl1 = flStrForObj(response[@"imageUrl1"]);
    self.imageUrl2 = flStrForObj(response[@"imageUrl2"]);
    self.imageUrl3 = flStrForObj(response[@"imageUrl3"]);
    self.imageUrl4 = flStrForObj(response[@"imageUrl4"]);
    
    self.cloudinaryPublicId = flStrForObj(response[@"cloudinaryPublicId"]);
    self.cloudinaryPublicId1 = flStrForObj(response[@"cloudinaryPublicId1"]);
    self.cloudinaryPublicId2 = flStrForObj(response[@"cloudinaryPublicId2"]);
    self.cloudinaryPublicId3 = flStrForObj(response[@"cloudinaryPublicId3"]);
    self.cloudinaryPublicId4 = flStrForObj(response[@"cloudinaryPublicId4"]);
    
    self.thumbnailImageUrl = flStrForObj(response[@"thumbnailImageUrl"]);
    self.likes = flStrForObj(response[@"likes"]);
    self.city = flStrForObj(response[@"city"]);
    self.latitude = [flStrForObj(response[@"latitude"]) floatValue];
    self.longitude = [flStrForObj(response[@"longitude"]) floatValue];
    self.countrySname = flStrForObj(response[@"countrySname"]);
    self.userFullName = flStrForObj(response[@"userFullName"]);
    self.username = flStrForObj(response[@"username"]);
    self.profilePicUrl =  flStrForObj(response[@"profilePicUrl"]);
    self.userId = flStrForObj(response[@"userId"]);
    self.userMqttId = flStrForObj(response[@"userMqttId"]);
    self.memberFullName = flStrForObj(response[@"userMqttId"]);
    self.membername = flStrForObj(response[@"membername"]);
    self.memberFullName = flStrForObj(response[@"memberFullName"]);
    self.memberMqttId = flStrForObj(response[@"memberMqttId"]);
    self.memberId = flStrForObj(response[@"memberId"]);
    self.memberProfilePicUrl = flStrForObj(response[@"memberProfilePicUrl"]);
    self.place = flStrForObj(response[@"place"]);
    self.postId = flStrForObj(response[@"postId"]);
    self.postedOn = flStrForObj(response[@"postedOn"]);
    self.likedByUsers = [[NSMutableArray alloc ]initWithArray:response[@"likedByUsers"]];
    self.message  = flStrForObj(response[@"message"]) ;
    self.likeStatus = [flStrForObj(response[@"likeStatus"]) boolValue];
    self.negotiable = [flStrForObj(response[@"negotiable"]) boolValue];
    self.postedByUserName = flStrForObj(response[@"postedByUserName"]);
    self.imageCount = [flStrForObj(response[@"imageCount"]) integerValue];
    self.sold = [flStrForObj(response[@"sold"]) integerValue];
    
    self.imageHeight = [flStrForObj(response[@"containerHeight"])integerValue];
    self.imageWidth = [flStrForObj(response[@"containerWidth"])integerValue];
    
    self.isPromoted = [flStrForObj(response[@"isPromoted"]) boolValue];
    self.propertyArray = [Filter arrayOfFilters:response[@"postFilter"]];
    
    self.isSwap = [flStrForObj(response[@"isSwap"]) boolValue];
    
    self.swapPosts = [PostSuggession arrayOfPostSuggessions:response[@"swapPost"]];
    
    return self;
}


+(NSMutableArray *) arrayOfProducts :(NSArray *)responseData
{
    NSMutableArray *productsArray = [[NSMutableArray alloc]init];
    for(NSDictionary *productDict in responseData) {
        
        ProductDetails *product = [[ProductDetails alloc]initWithDictionary:productDict] ;
        [productsArray addObject:product];
    }
    
    return productsArray ;
}

@end
