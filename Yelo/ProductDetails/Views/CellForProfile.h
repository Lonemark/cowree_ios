//
//  CellForProfile.h

//
//  Created by Rahul Sharma on 13/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetails.h"

@interface CellForProfile : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *labelForUserName;
@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UIButton *buttonFollowOutlet;

-(void)setObjectForData:(ProductDetails *)product andDataFrom:(BOOL)flag;
@end
