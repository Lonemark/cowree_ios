//
//  CellForProfile.m

//
//  Created by Rahul Sharma on 13/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CellForProfile.h"

@implementation CellForProfile

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}



-(void)setObjectForData:(ProductDetails *)product andDataFrom:(BOOL)flag{

    [self.profileImageView sd_setImageWithURL:[NSURL URLWithString: product.memberProfilePicUrl] placeholderImage:[UIImage imageNamed:@"itemUserDefault"]];
    self.labelForUserName.text = product.membername ;
    
    if([[Helper userName]isEqualToString:product.membername] || flag)
    {
        self.buttonFollowOutlet.hidden = YES;
    }
    else{
        self.buttonFollowOutlet.hidden = NO;
        if(product.followRequestStatus) {
            [self.buttonFollowOutlet makeButtonAsFollowing];
        }
        else {
            [self.buttonFollowOutlet makeButtonAsFollow];
        }
    }

}
@end
