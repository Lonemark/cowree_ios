//
//  LikeCommentTableViewCell.m
//  InstaVideoPlayerExample
//
//  Created by Rahul Sharma on 13/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "LikeCommentTableViewCell.h"
#import "LikersCollectionViewCell.h"
#import "ProfileViewController.h"
@implementation LikeCommentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.likersCollectionViewOutlet.dataSource = self;
    self.likersCollectionViewOutlet.delegate = self;
    self.likeButtonOutlet.layer.borderWidth = 1;
    self.likeButtonOutlet.layer.borderColor = [UIColor colorWithRed:145/255.0 green:145/255.0 blue:145/255.0 alpha:.7].CGColor;
    self.likeButtonOutlet.layer.cornerRadius = 3;
    self.viewCountButton.layer.borderColor = [UIColor colorWithRed:145/255.0 green:145/255.0 blue:145/255.0 alpha:.7].CGColor ;
    [self.likeButtonOutlet buttonImageInsets:5];
    [self.viewCountButton buttonImageInsets:5];
    [self.commentButtonOutlet buttonImageInsets:20];
    [self.shareButtonOutlet buttonImageInsets:20];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setPropertiesOfOutletscheckFlag:(BOOL)flag
{
    if(flag)
    {
        self.likeButtonOutlet.hidden = YES;
        [self.activityIndicatorOutlet startAnimating];
        self.activityIndicatorOutlet.hidden = NO ;
    }
    else
    {
        self.likeButtonOutlet.hidden = NO;
        [self.activityIndicatorOutlet stopAnimating];
    }
    
    [self.likersCollectionViewOutlet reloadData];
    
    [self.viewCountButton setTitle:self.product.clickCount forState:UIControlStateNormal];
    
}

-(void)setPriceCurrencyNameOfProduct:(ProductDetails *)product
{
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:product.currency];
    NSString *currencySymbol = [NSString stringWithFormat:@"%@",[locale displayNameForKey:NSLocaleCurrencySymbol value:product.currency]];
    
    self.labelForPrice.text = product.price;
    self.labelForCurrency.text = currencySymbol;
    self.labelForTitle.text = product.productName ;
    if(product.isSwap)
    {
    NSMutableArray *swapPostsName = [NSMutableArray new];
    for (PostSuggession *swapPost in product.swapPosts) {
        [swapPostsName addObject:swapPost.productName];
    }
    self.swapForLabel.text = [NSString stringWithFormat:@"Swap For : %@",[swapPostsName componentsJoinedByString:@", "]];
    }
    else{
        self.swapForLabel.text = @"";
    }
    
}

#pragma mark - CollectionView DataSource Method

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.arrayOfProfilePics.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{ 
    LikersCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:self.collectionViewCellID forIndexPath:indexPath];
    cell.userProfileImageView.clipsToBounds = YES;
    cell.userProfileImageView.layer.cornerRadius = 14;
    NSURL *url = [NSURL URLWithString:flStrForObj(self.arrayOfProfilePics[indexPath.row][@"profilePicUrl"])];
    [cell.userProfileImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultpp"]];
    [cell.userProfileImageView setTransform:CGAffineTransformMakeScale(1, 1)];
    return cell;
}

#pragma mark - CollectionView Delegate Method

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(28,28);
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0,0,0,0);
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return -5;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    ProfileViewController *newView = [storyboard instantiateViewControllerWithIdentifier:@"userProfileStoryBoardId"];
     newView.memberName = self.product.likedByUsers[indexPath.row][@"likedByUsers"];
    newView.isMemberProfile = YES;
    newView.productDetails = YES;
    newView.hidesBottomBarWhenPushed = YES ;
   [self.navController pushViewController:newView animated:YES];
}


@end
