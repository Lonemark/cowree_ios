//
//  LanguagesViewController.h
//  MobiVenta
//
//  Created by Rahul Sharma on 11/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguagesViewController : UIViewController
- (IBAction)doneButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *languageDoneButton;

    
@end
