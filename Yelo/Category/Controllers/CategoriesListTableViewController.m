

//
//  CategoriesListTableViewController.m

//
//  Created by Rahul Sharma on 14/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CategoriesListTableViewController.h"
#import "cellForListOfOptions.h"
#import "SubCategoryTableViewController.h"
#import "FilterFieldsViewController.h"
#import "PostListingsViewController.h"

@interface CategoriesListTableViewController ()<WebServiceHandlerDelegate , UITableViewDelegate,UITableViewDataSource>

@end

@implementation CategoriesListTableViewController

#pragma
#pragma mark- ViewController LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNavLeftButton];
    if([self.navigationItem.title isEqualToString:NSLocalizedString(navTitleForProductCategory, navTitleForProductCategory)]){
        self.dataArray = [NSMutableArray new];
        self.tableViewForList.separatorColor = [UIColor clearColor];
        ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];  [HomePI showPIOnView:self.view withMessage:NSLocalizedString(LoadingIndicatorTitle, LoadingIndicatorTitle)];
        if([self.showResultsFor isEqualToString:@"categories"])
            [self requestForListWithoutInfinteRefresh];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 Create left navbar button
 */
-(void)createNavLeftButton
{
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName normalState:mNavigationBackButtonImageName];
    [navLeft rotateButton];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,20,20)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(navLeftButton) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
}

-(void)navLeftButton
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Request ForPost

/**
 Method call first time to get likes details upto 20
 */
-(void)requestForListWithoutInfinteRefresh {
    self.currentIndex = 0;
    [self requestListWithIndex:self.currentIndex];
}


/**
 WebService Call with Request params.
 */
-(void)requestListWithIndex :(NSInteger )offsetIndex {
    NSDictionary *requestDict = @{
                                  mLimit : mLimitValue,
                                  moffset :[NSNumber numberWithInteger:20 * offsetIndex],
                                  mLanguageCode:[Helper currentLanguage]
                                  };
    [WebServiceHandler getCategories:requestDict andDelegate:self];
}

-(void)requestForListBasedOnRequirement {
    
    //for Home Screen.
    //requestingForPosts.
    __weak CategoriesListTableViewController *weakSelf = self;
    // setup infinite scrollinge
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        if (self.dataArray.count >19) {
            [weakSelf requestListWithIndex:weakSelf.currentIndex];
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if([self.showResultsFor isEqualToString:@"condition"])
    {
        return mArrayOfConditions.count;
    }
    return self.dataArray.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    cellForListOfOptions *cell = [tableView dequeueReusableCellWithIdentifier:@"cellForList" forIndexPath:indexPath];
    
    
    if ([self.showResultsFor isEqualToString:@"categories"]) {
        [cell setCategoriesList:self.dataArray[indexPath.row] previousSelection:self.previousSelection];
        
    }
    else {
        [cell setCondition:mArrayOfConditions IndexPath:indexPath andPreviousSelection:self.previousSelection];
    }
    return cell;
}


#pragma mark - Table view delegate method

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.showResultsFor isEqualToString:@"categories"])
    {
        Category *category = self.dataArray[indexPath.row];
        if(category.subCategoryCount){
            SubCategoryTableViewController *subVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SubCategoryStoryboard"];
            subVC.categoryName = category.name;
            subVC.categoryObj = category;
            subVC.listing = self.listing;
            [self.navigationController pushViewController:subVC animated:YES];
        }
        else if (category.filterCount)
        {
            // Case for no subcategory.
            FilterFieldsViewController *filterFieldsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SubcategoryFilterFieldsStoryboard"];
            filterFieldsVC.filterKeyValueDict = [NSMutableDictionary new];
            
            cellForListOfOptions *categoryCell = [tableView cellForRowAtIndexPath:indexPath];
            if(!categoryCell.imageViewForSelection.hidden)
            {
                [filterFieldsVC.filterKeyValueDict addEntriesFromDictionary:self.listing.filter];
            }
            filterFieldsVC.filter = category.filterArray;
            filterFieldsVC.subcategoryObj = [[SubCategory alloc]init];
            filterFieldsVC.categoryObj = category;
            [self.navigationController pushViewController:filterFieldsVC animated:YES];
        }
        else
        {
            // Case For category only.
            for (UIViewController *viewController in [self.navigationController viewControllers]) {
                if([viewController isKindOfClass:[PostListingsViewController class]]){
                    PostListingsViewController *newVC = (PostListingsViewController *)viewController;
                    Category *category = self.dataArray[indexPath.row];
                    newVC.listings.category = category.name;
                    newVC.listings.categoryId = category.categoryNodeId;
                    newVC.listings.subCategoryNodeId = category.subCategoryNodeId;
                    
                    newVC.listings.subCategory = @"";
                    newVC.listings.filter = nil ;
                    [newVC.tableViewForListings reloadData];
                    [self.navigationController popToViewController:viewController animated:YES];
                }
            }
            
        }
    }
    else if([self.showResultsFor isEqualToString:@"condition"])
    {
        self.callBack(nil,mArrayOfConditions[indexPath.row]);
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}


#pragma mark - Web Service Delegate method
-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (error) {
        [Helper showAlertWithTitle:@"Error" Message:[error localizedDescription] viewController:self];
        return;
    }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    
    if (requestType == RequestTypeGetCategories) {
        
        
        //success response(200 is for success code).
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                //                self.currentIndex = 0;
                [self handlingResponseOfExplorePosts:response];
            }
                break;
            case 204:{
                
            }
                break;
                
            default:
                break;
        }
    }
    
}

/**
 Handling the response comes from API call.
 
 @param response Dictionary.
 */
-(void)handlingResponseOfExplorePosts :(NSDictionary *) response
{
    self.tableViewForList.separatorColor = mTableViewCellSepratorColor;
    
    if(self.currentIndex == 0) {
        [self.dataArray removeAllObjects];
        self.dataArray = [Category arrayOfCategory:response[@"data"]];
    }
    else {
        self.currentIndex++;
        NSArray *categoryWithPaging = [Category arrayOfCategory:response[@"data"]];
        [self.dataArray addObjectsFromArray:categoryWithPaging];
    }
    [self.tableViewForList reloadData];
    
    if(self.dataArray.count >19)
        [self requestListWithIndex:self.currentIndex];
}





@end

