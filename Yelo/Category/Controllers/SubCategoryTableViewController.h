//
//  SubCategoryTableViewController.h
//  CollegeStax
//
//  Created by 3Embed on 18/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubCategoryTableViewController : UITableViewController

@property(weak,nonatomic)Listings *listing;
@property (strong, nonatomic) IBOutlet UITableView *tableViewOutlet;

@property (strong, nonatomic) NSString * categoryName;
@property (strong, nonatomic) Category *categoryObj;


@property (weak, nonatomic) IBOutlet UIButton *backButtonOutlet;

- (IBAction)backButtonButtonAction:(id)sender;

@end
