//
//  SubCategory.m
//  CollegeStax
//
//  Created by 3Embed on 18/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "SubCategory.h"

@implementation SubCategory

-(instancetype)initWithDictionary:(NSDictionary *)response
{
    self = [super init];
    if (!self) { return nil; }
    
    self.fieldCount = [flStrForObj(response[@"fieldCount"])integerValue];
    self.image = flStrForObj(response[@"imageUrl"]);
    self.subCategoryName = [flStrForObj(response[@"subCategoryName"]) capitalizedString];
    self.subCategoryNodeId = flStrForObj(response[@"subCategoryNodeId"]);
    self.languageCode = [Helper currentLanguage];
    self.filterArray = [NSMutableArray new];
    self.filterArray = [Filter arrayOfFilters:response[@"filter"]];
    return self;
}


+(NSMutableArray *) arrayOfSubCategory :(NSArray *)responseData
{
    NSMutableArray *subCategoryArray = [[NSMutableArray alloc]init];
    for(NSDictionary *subCategoryDic in responseData) {
        
        SubCategory *subCategory = [[SubCategory alloc]initWithDictionary:subCategoryDic] ;
        [subCategoryArray addObject:subCategory];
    }
    
    return subCategoryArray ;
}



@end
