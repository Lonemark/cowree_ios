//
//  SubCategory.h
//  CollegeStax
//
//  Created by 3Embed on 18/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Filter.h"

@interface SubCategory : NSObject

@property(nonatomic)NSUInteger fieldCount;
@property(nonatomic,copy)NSString *image;
@property(nonatomic,copy)NSString *subCategoryName;
@property(nonatomic,copy)NSString *subCategoryNodeId;
@property(nonatomic)NSString *subId;

@property(nonatomic,copy)NSString *languageCode;

@property(nonatomic,retain)NSMutableArray *filterArray;

- (instancetype)initWithDictionary:(NSDictionary *)response;
+(NSMutableArray *) arrayOfSubCategory :(NSArray *)responseData ;
@end
