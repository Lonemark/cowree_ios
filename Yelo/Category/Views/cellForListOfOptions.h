//
//  cellForListOfOptions.h

//
//  Created by Rahul Sharma on 14/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Category.h"


@interface cellForListOfOptions : UITableViewCell
;

@property (strong, nonatomic) IBOutlet UILabel *labelForList;
@property (weak, nonatomic) IBOutlet UILabel *conditionLabel;


@property (strong, nonatomic) IBOutlet UIImageView *imageViewForSelection;
@property (weak, nonatomic) IBOutlet UIImageView *categoryImage;

-(void)setCategoriesList :(Category *)category previousSelection :(NSString *)previousSelection;

-(void)setCondition :(NSArray *)dataArray IndexPath :(NSIndexPath *)indexpath andPreviousSelection:(NSString *)previousSelection;

@end

