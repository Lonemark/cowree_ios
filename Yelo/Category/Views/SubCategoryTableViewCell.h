//
//  SubCategoryTableViewCell.h
//  CollegeStax
//
//  Created by 3Embed on 18/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubCategory.h"
#import "Filter.h"



@interface SubCategoryTableViewCell : UITableViewCell

@property(weak,nonatomic)NSString *previousSelection;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *tickMarkImage;

@property (weak, nonatomic) IBOutlet UIImageView *subCategoryImage;


-(void)setSubCategoryNames :(SubCategory *)subcategory;

-(void)setFieldsForFilter :(Filter *)filter previousSelection :(NSDictionary *)previousSet;

-(void)setValuesForFilter :(NSString *)value previousSelection :(NSString *)previousValue;

-(void)checkSelectedKeyValueFilter :(NSMutableArray *)keyValueArray forValue :(NSString *)value fieldName:(NSString *)fieldName;

@property (weak, nonatomic) IBOutlet UILabel *mandatory;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;

@end
