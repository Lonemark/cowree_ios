//
//  CustomImageView.m
//  Jaiecom
//
//  Created by Rahulsharma on 05/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "CustomImageView.h"
//#import "AppData.h"

@implementation CustomImageView

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    
    if([[RTL sharedInstance] isRTL]) {
   self.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        }
    
}

@end
