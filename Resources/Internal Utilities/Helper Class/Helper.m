//
//  Helper.m

//
//  Created by Rahul Sharma on 9/3/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "Helper.h"
#import "FontDetailsClass.h"
#import  <Social/Social.h>
#import <Accounts/Accounts.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareVideo.h>
#import <FBSDKShareKit/FBSDKShareOpenGraphContent.h>
#import <FBSDKShareKit/FBSDKShareDialog.h>
#import <FBSDKShareKit/FBSDKShareLinkContent.h>
#import <FBSDKShareKit/FBSDKShareVideoContent.h>
#import <FBSDKLoginKit/FBSDKLoginManager.h>
#import <FBSDKShareKit/FBSDKShareAPI.h>
#import <FBSDKShareKit/FBSDKSharePhotoContent.h>
#import "LanguageManager.h"
#import "PGTabBar.h"

#define  ACCEPTABLE_CHARACTERSFORPRICE @"1234567890$₹."

@import Firebase;
@interface Helper ()

@end

@implementation Helper


/* -----------------------------------------------------------------------*/
#pragma mark
#pragma mark - TimeConverting  From EpochValue
/* ----------------------------------------------------------------------*/


+(NSString *)PostedTimeSincePresentTime:(NSTimeInterval)seconds {
    
    //converting seconds into minutes or hours or  days or weeks based on number of seconds.
    if(seconds < 60)
    {
        NSInteger time = round(seconds);
        //showing timestamp in seconds.
        
        if(seconds < 1)
        {
            seconds = 2;
        }
        NSString *secondsInstringFormat = [NSString stringWithFormat:@"%ld", (long)time];
        NSString *secondsWithSuffixS;
        if (time >1) {
            secondsWithSuffixS = [secondsInstringFormat stringByAppendingString:@" SECONDS AGO"];
        }
        else {
            secondsWithSuffixS = [secondsInstringFormat stringByAppendingString:@" SECOND AGO"];
        }
        
        return secondsWithSuffixS;
    }
    
    else if (seconds >= 60 && seconds <= 60 *60) {
        //showing timestamp in minutes.
        NSInteger numberOfMinutes = seconds / 60;
        NSString *minutesInstringFormat = [NSString stringWithFormat:@"%ld", (long)numberOfMinutes];
        NSString *minutesWithSuffixM;
        
        if (numberOfMinutes >1) {
            minutesWithSuffixM = [minutesInstringFormat stringByAppendingString:@" MINUTES AGO"];
        }
        else {
            minutesWithSuffixM = [minutesInstringFormat stringByAppendingString:@" MINUTE AGO"];
        }
        
        return minutesWithSuffixM;
    }
    else if (seconds >= 60 *60 && seconds <= 60*60*24) {
        //showing timestamp in hours.
        NSInteger numberOfHours = seconds /(60*60);
        
        NSString *hoursInstringFormat = [NSString stringWithFormat:@"%ld", (long)numberOfHours];
        NSString *hoursWithSuffixH;
        if (numberOfHours >1) {
            hoursWithSuffixH = [hoursInstringFormat stringByAppendingString:@" HOURS AGO"];
        }
        else {
            hoursWithSuffixH = [hoursInstringFormat stringByAppendingString:@" HOUR AGO"];
        }
        
        return hoursWithSuffixH;
    }
    else if (seconds >= 24 *60 *60 && seconds <= 60*60*24*7) {
        //showing timestamp in days.
        NSInteger numberOfDays = seconds/(60*60*24);
        NSString *daysInstringFormat = [NSString stringWithFormat:@"%ld", (long)numberOfDays];
        NSString *daysWithSuffix;
        if (numberOfDays >1) {
            daysWithSuffix = [daysInstringFormat stringByAppendingString:@" DAYS AGO"];
        }
        else {
            daysWithSuffix = [daysInstringFormat stringByAppendingString:@" DAY AGO"];
        }
        return daysWithSuffix;
    }
    else if (seconds >= 60*60*24*7) {
        //showing timestamp in weeks.
        NSInteger numberOfWeeks = seconds /(60*60*24*7);
        NSString *weeksInstringFormat = [NSString stringWithFormat:@"%ld", (long)numberOfWeeks];
        NSString *weeksWithSuffixS;
        if (numberOfWeeks >1) {
            weeksWithSuffixS = [weeksInstringFormat stringByAppendingString:@" WEEKS AGO"];
        }
        else {
            weeksWithSuffixS = [weeksInstringFormat stringByAppendingString:@" WEEK AGO"];
        }
        return weeksWithSuffixS;
    }
    else if (seconds >= 60*60*24*7*4 && seconds <= 60*60*24*7*4*12 )
    {
        //showing timestamp in weeks.
        NSInteger numberOfWeeks = seconds /(60*60*24*7*4);
        NSString *monthsInstringFormat = [NSString stringWithFormat:@"%ld", (long)numberOfWeeks];
        NSString *weeksWithSuffixS;
        if(numberOfWeeks > 1){
        weeksWithSuffixS = [monthsInstringFormat stringByAppendingString:@" MONTHS AGO"];
        }
        else{
        weeksWithSuffixS = [monthsInstringFormat stringByAppendingString:@" MONTH AGO"];
        }
            
        return weeksWithSuffixS;
    }
    else if (seconds >= 60*60*24*7*4*12)
    {
        //showing timestamp in weeks.
        NSInteger numberOfWeeks = seconds /(60*60*24*7*4*12);
        NSString *yearInstringFormat = [NSString stringWithFormat:@"%ld", (long)numberOfWeeks];
        NSString *weeksWithSuffixS;
        
        if(numberOfWeeks > 1){
        weeksWithSuffixS = [yearInstringFormat stringByAppendingString:@" YEARS AGO"];
        }
        else{
         weeksWithSuffixS = [yearInstringFormat stringByAppendingString:@" YEAR AGO"];
        }
        return weeksWithSuffixS;
    }
    return @"";
}

/**
 This method is invoked to get the duration between posted time to the present time.
 
 @param epochTime epoch time stamp.
 @return duration time.
 */
+(NSString *)convertEpochToNormalTime :(NSString *)epochTime{
    //getting date(including time) from epochTime.
    
    // Convert NSString to NSTimeInterval
    NSTimeInterval seconds = [epochTime doubleValue];
    // (Step 1) Create NSDate object
    NSDate *epochNSDate = [[NSDate alloc] initWithTimeIntervalSince1970:(seconds/1000)];
    // (Step 2) Use NSDateFormatter to display epochNSDate in local time zone
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    
    //getting present time.
    
    NSDate *todayDate = [NSDate date]; // get today date
    NSDateFormatter *dateFormatte = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatte setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"]; //Here we can set the format which we need
    //getting duration between posted time to the present time.
    NSTimeInterval secondsBetween = [todayDate timeIntervalSinceDate:epochNSDate];
    NSString *timeStamp = [Helper PostedTimeSincePresentTime:secondsBetween];
    return timeStamp;
}

#pragma mark - Paypal Methods

/**
 Get paypal link for user.
 
 @return paypal link.
 */
+(NSString *)getPayPalLink {
    NSString *pyPalLink = [[NSUserDefaults standardUserDefaults] valueForKey:payPalLink];
    return pyPalLink;
}

/**
 Store PayPal link for logged user.
 
 @param paypalLink paypal.
 */
+(void)storePaypalLinkWithPaypalLink : (NSString *)paypalLink {
    [[NSUserDefaults standardUserDefaults] setObject:paypalLink forKey:payPalLink];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/* -----------------------------------------------------------------------*/
#pragma mark
#pragma mark - TimeConverting  From EpochValue
/* ----------------------------------------------------------------------*/

//converting seconds into minutes or hours or  days or weeks based on number of seconds.
+(NSString *)PostedTimeSincePresentTimeInShort:(NSTimeInterval)seconds {
    if(seconds < 60)
    {
        NSInteger time = round(seconds);
        //showing timestamp in seconds.
        
        if(seconds < 1)
        {
            seconds = 2;
        }
        NSString *secondsInstringFormat = [NSString stringWithFormat:@"%ld", (long)time];
        NSString *secondsWithSuffixS;
        
        secondsWithSuffixS = [secondsInstringFormat stringByAppendingString:@"s"];
        
        return secondsWithSuffixS;
    }
    
    else if (seconds >= 60 && seconds <= 60 *60) {
        //showing timestamp in minutes.
        NSInteger numberOfMinutes = seconds / 60;
        NSString *minutesInstringFormat = [NSString stringWithFormat:@"%ld", (long)numberOfMinutes];
        NSString *minutesWithSuffixM;
        
        minutesWithSuffixM = [minutesInstringFormat stringByAppendingString:@"m"];
        
        return minutesWithSuffixM;
    }
    else if (seconds >= 60 *60 && seconds <= 60*60*24) {
        //showing timestamp in hours.
        NSInteger numberOfHours = seconds /(60*60);
        
        NSString *hoursInstringFormat = [NSString stringWithFormat:@"%ld", (long)numberOfHours];
        NSString *hoursWithSuffixH;
        hoursWithSuffixH = [hoursInstringFormat stringByAppendingString:@"h"];
        
        return hoursWithSuffixH;
    }
    else if (seconds >= 24 *60 *60 && seconds <= 60*60*24*7) {
        //showing timestamp in days.
        NSInteger numberOfDays = seconds/(60*60*24);
        NSString *daysInstringFormat = [NSString stringWithFormat:@"%ld", (long)numberOfDays];
        NSString *daysWithSuffix;
        daysWithSuffix = [daysInstringFormat stringByAppendingString:@"d"];
        return daysWithSuffix;
    }
    else if (seconds >= 60*60*24*7 && seconds <= 60*60*24*7*4) {
        //showing timestamp in weeks.
        NSInteger numberOfWeeks = seconds /(60*60*24*7);
        NSString *weeksInstringFormat = [NSString stringWithFormat:@"%ld", (long)numberOfWeeks];
        NSString *weeksWithSuffixS;
        weeksWithSuffixS = [weeksInstringFormat stringByAppendingString:@"w"];
        return weeksWithSuffixS;
    }
    
    else if (seconds >= 60*60*24*7*4 && seconds <= 60*60*24*7*4*12 )
    {
        //showing timestamp in weeks.
        NSInteger numberOfWeeks = seconds /(60*60*24*7*4);
        NSString *monthsInstringFormat = [NSString stringWithFormat:@"%ld", (long)numberOfWeeks];
        NSString *weeksWithSuffixS;
        if(numberOfWeeks > 1){
        weeksWithSuffixS = [monthsInstringFormat stringByAppendingString:@"Months"];
        }
        else
        {
        weeksWithSuffixS = [monthsInstringFormat stringByAppendingString:@"Month"];
        }
        return weeksWithSuffixS;
    }
    else if (seconds >= 60*60*24*7*4*12)
    {
        //showing timestamp in weeks.
        NSInteger numberOfWeeks = seconds /(60*60*24*7*4*12);
        NSString *yearInstringFormat = [NSString stringWithFormat:@"%ld", (long)numberOfWeeks];
        NSString *weeksWithSuffixS;
        if(numberOfWeeks > 1){
        weeksWithSuffixS = [yearInstringFormat stringByAppendingString:@"Years"];
        }
        else
        {
        weeksWithSuffixS = [yearInstringFormat stringByAppendingString:@"Year"];
        }
        return weeksWithSuffixS;
    }
    return @"";
}

+(NSString *)convertEpochToNormalTimeInshort :(NSString *)epochTime{
    //getting date(including time) from epochTime.
    
    // Convert NSString to NSTimeInterval
    NSTimeInterval seconds = [epochTime doubleValue];
    // (Step 1) Create NSDate object
    NSDate *epochNSDate = [[NSDate alloc] initWithTimeIntervalSince1970:(seconds/1000)];
    // (Step 2) Use NSDateFormatter to display epochNSDate in local time zone
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    
    //getting present time.
    
    NSDate *todayDate = [NSDate date]; // get today date
    NSDateFormatter *dateFormatte = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatte setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"]; //Here we can set the format which we need
    //getting duration between posted time to the present time.
    NSTimeInterval secondsBetween = [todayDate timeIntervalSinceDate:epochNSDate];
    NSString *timeStamp = [Helper PostedTimeSincePresentTimeInShort:secondsBetween];
    return timeStamp;
}


/* -----------------------------------------------------------------------*/
#pragma mark
#pragma mark - Measure Label Height -
/* ----------------------------------------------------------------------*/

+ (CGFloat)measureHieightLabel: (UILabel *)label
{
    CGSize constrainedSize = CGSizeMake(label.frame.size.width  , 9999);
    CGRect requiredHeight=CGRectMake(0, 0, 0, 0);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    if (label.text) {
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:label.text attributes:attributesDictionary];
        
        requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        
        if (requiredHeight.size.width > label.frame.size.width) {
            requiredHeight = CGRectMake(0,0, label.frame.size.width, requiredHeight.size.height);
        }
    }
    
    CGRect newFrame = label.frame;
    newFrame.size.height = requiredHeight.size.height;
    return  newFrame.size.height;
}


#pragma mark - customizing cell activity statements

+(NSMutableAttributedString*)customisedActivityStmt:(NSString*)username :(NSString*)statment {
    
    
    
    NSString *testString= statment;
    
    NSRange range = [testString rangeOfString:username];
    
     
    NSMutableAttributedString * attributtedComment = [[NSMutableAttributedString alloc] initWithString:statment];
    
    [attributtedComment addAttribute:NSForegroundColorAttributeName
                               value:[UIColor colorWithRed:0.1176 green:0.1176 blue:0.1176 alpha:1.0]
                               range:range];
    
    [attributtedComment addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:RobotoMedium size:14]
                               range:range];
    
    
    
    return attributtedComment;
}

+(NSMutableAttributedString*)customisedActivityStmt:(NSString*)username seconUserName:(NSString *)secondUserName  timeForPost:(NSString *)time : (NSString*)statment {
    
    NSRange range = [statment rangeOfString:username];
    
    NSRange seconUserNameRage = [statment rangeOfString:secondUserName];
    
    NSRange rangeForTime = [statment rangeOfString:time];
    
    
    NSMutableAttributedString * attributtedComment = [[NSMutableAttributedString alloc] initWithString:statment];
    
    [attributtedComment addAttribute:NSForegroundColorAttributeName
                               value:[UIColor colorWithRed:0.1176 green:0.1176 blue:0.1176 alpha:1.0]
                               range:range];
    
    [attributtedComment addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:RobotoMedium size:14]
                               range:range];
    
    [attributtedComment addAttribute:NSForegroundColorAttributeName
                               value:[UIColor colorWithRed:0.1176 green:0.1176 blue:0.1176 alpha:1.0]
                               range:seconUserNameRage];
    
    [attributtedComment addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:RobotoMedium size:14]
                               range:seconUserNameRage];
    
    
    [attributtedComment addAttribute:NSForegroundColorAttributeName
                               value:[UIColor lightGrayColor]
                               range:rangeForTime];
    
    [attributtedComment addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:RobotoRegular size:14]
                               range:rangeForTime];
    
    return attributtedComment;
}




#pragma marks - customizing lable and button
+(void)setToLabel:(UILabel*)lbl Text:(NSString*)txt WithFont:(NSString*)font FSize:(float)_size Color:(UIColor*)color
{
    
    lbl.textColor = color;
    
    if (txt != nil) {
        lbl.text = NSLocalizedString(txt,txt) ;
    }
    
    
    if (font != nil) {
        lbl.font = [UIFont fontWithName:font size:_size];
    }
    
}

+(void)setButton:(UIButton*)btn Text:(NSString*)txt WithFont:(NSString*)font FSize:(float)_size TitleColor:(UIColor*)t_color ShadowColor:(UIColor*)s_color
{
    [btn setTitle:txt forState:UIControlStateNormal];
    
    [btn setTitleColor:t_color forState:UIControlStateNormal];
    
    if (s_color != nil) {
        [btn setTitleShadowColor:s_color forState:UIControlStateNormal];
    }
    
    
    if (font != nil) {
        btn.titleLabel.font = [UIFont fontWithName:font size:_size];
    }
    else
    {
        btn.titleLabel.font = [UIFont systemFontOfSize:_size];
    }
}

/* -----------------------------------------------------------------------*/
#pragma mark
#pragma mark - User Details -
/* ----------------------------------------------------------------------*/


/**
 This method is invoked to store user details on login/Signup.
 
 @param user UserDetails Model Object.
 */
+(void)storeUserLoginDetails:(UserDetails *)user
{
    [[NSUserDefaults standardUserDefaults]setObject:user.userId forKey:mUserId];
    [[NSUserDefaults standardUserDefaults]setObject:user.token forKey:mauthToken];
    [[NSUserDefaults standardUserDefaults]setObject:user.username forKey:mUserName];
    [[NSUserDefaults standardUserDefaults]setObject:user.mqttId forKey:mMqttId];
    [[NSUserDefaults standardUserDefaults]setObject:user.profilePicUrl forKey:mProfileUrl];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 Get UserName on the basis of login.
 
 @return logged in user name.
 */
+(NSString *)userName {
    
    NSString *userName = [[NSUserDefaults standardUserDefaults]objectForKey:mUserName] ;
    return userName;
}

/**
 Method to get the MQTTID.
 
 @return MQTTID of logged user.
 */
+(NSString *)getMQTTID {
    NSString *mqttId = [[NSUserDefaults standardUserDefaults] objectForKey:mMqttId];
    //if user first time login(registration) then userData dictonary will be empty and user login then userData will contain data.
    
    if(!mqttId) {
        return  @"mqttId";
    }
    if ([mqttId isEqual:[NSNull null]])
    {
        return @"mqttId";
    }
    return mqttId;
}

/**
 Method to get the authToken.
 
 @return return auth token for logged user.
 */
+(NSString *)userToken {
    //if user first time login(registration) then userData dictonary will be empty and user login then userData will contain data.
    
    NSString *userToken = [[NSUserDefaults standardUserDefaults]objectForKey:mauthToken] ;
    if(!userToken) {
        return  mGuestToken;
    }
    return userToken;
}

/**
 Method to get the push token.
 
 @return push token for logged user.
 */
+(NSString *)deviceToken{
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults]objectForKey:mdeviceToken];
    
    if (!(deviceToken.length >1)) {
        deviceToken = @"failed To fetch Push Token";
    }
    return deviceToken;
}

+(void)disableNotificationForUser :(NSString *)currentUserId andProductId :(NSString *)productId{
    [[NSUserDefaults standardUserDefaults]setObject:currentUserId forKey:@"currentUserId"];
    [[NSUserDefaults standardUserDefaults]setObject:productId forKey:@"productId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void)EnableNotificationForUser
{   [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"currentUserId"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"productId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 Get logged user profile pic Url in string format.
 
 @return profile pic url string.
 */
+(NSString *)userProfileImageUrl {
    return [[NSUserDefaults standardUserDefaults] objectForKey:mProfileUrl];
}

#pragma mark -
#pragma marks - Get Web Link -

/**
 *  This method is to convert media link into tinylink
 */

+ (NSString*)getWebLinkForFeed:(NSString *)link
{
    NSString *shortUrl;
    NSString *apiEndpoint;
    NSString *originallink = link ;
    if (adminGalleryURL.length)
    {
        NSLog(@"Sharing with adminLink");
        NSString *link = [NSString stringWithFormat:@"%@/%@/%@",adminGalleryURL,@"userId",@"postId"];
        //NSString *link = [NSString stringWithFormat:@"%@=%@&uid=%@",adminGalleryURL,@"userId",@"postId"]
        apiEndpoint = [NSString stringWithFormat:@"http://tinyurl.com/api-create.php?url=%@",link];
        shortUrl = [NSString stringWithContentsOfURL:[NSURL URLWithString:apiEndpoint]
                                            encoding:NSASCIIStringEncoding
                                               error:nil];
    } else {
        apiEndpoint = [NSString stringWithFormat:@"http://tinyurl.com/api-create.php?url=%@",originallink];
        shortUrl = [NSString stringWithContentsOfURL:[NSURL URLWithString:apiEndpoint]
                                            encoding:NSASCIIStringEncoding
                                               error:nil];
    }
    return shortUrl;
}

/*-------------------------------------------------------*/
#pragma mark
#pragma mark - labelHeightDynamically.
/*------------------------------------------------------*/

+(CGFloat )heightOfText:(UILabel *)label {
    
    //Calculate the expected size based on the font and linebreak mode of your label
    // FLT_MAX here simply means no constraint in height
    
    CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
    CGRect textRect = [label.text boundingRectWithSize:maximumLabelSize
                                                      options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
                                                   attributes:@{NSFontAttributeName:label.font}
                                                      context:nil];
//    CGSize expectedLabelSize = [label.text sizeWithFont:label.font constrainedToSize:maximumLabelSize lineBreakMode:label.lineBreakMode];
    CGRect newFrame = label.frame;
    newFrame.size.height = textRect.size.height;
    label.frame = newFrame;
    CGFloat dynamicHeightOfLabel = newFrame.size.height;
    return dynamicHeightOfLabel;
}


/**
 Facebook Sharing Method.
 
 @param product Product Model Object.
 */
+ (void)makeFBPostWithParams:(ProductDetails *)product
{
    NSString *url =  [SHARE_LINK stringByAppendingString:[NSString stringWithFormat:@"%@",product.postId]];
    
    NSString *originalLink = [SHARE_LINK  stringByAppendingString:product.postId] ;
    NSURL *link = [NSURL URLWithString:originalLink];
    FIRDynamicLinkComponents *components =
    [FIRDynamicLinkComponents componentsWithLink:link
                                          domain:mDomainForDeepLinking];
    
    FIRDynamicLinkSocialMetaTagParameters *socialParams = [FIRDynamicLinkSocialMetaTagParameters parameters];
    socialParams.title = product.productName ;
    socialParams.descriptionText = product.productDescription ;
    socialParams.imageURL = [NSURL URLWithString:product.mainUrl] ;
    components.socialMetaTagParameters = socialParams;
    
    
    FIRDynamicLinkIOSParameters *iosParams = [FIRDynamicLinkIOSParameters parametersWithBundleID:mIosBundleId];
    iosParams.fallbackURL = [NSURL URLWithString: url] ;
    components.iOSParameters = iosParams;
    
    FIRDynamicLinkAndroidParameters *androidParam = [FIRDynamicLinkAndroidParameters parametersWithPackageName:mAndroidBundleId];
    androidParam.fallbackURL = [NSURL URLWithString: url] ;
    components.androidParameters = androidParam;
    
    FIRDynamicLinkNavigationInfoParameters *navigationInfoParameters = [FIRDynamicLinkNavigationInfoParameters parameters];
    navigationInfoParameters.forcedRedirectEnabled = 0;
    components.navigationInfoParameters = navigationInfoParameters;
    
    [components shortenWithCompletion:^(NSURL *_Nullable shortURL,
                                        NSArray *_Nullable warnings,
                                        NSError *_Nullable error) {
        // Handle shortURL or error.
        if (error) {
            NSLog(@"Error generating short link: %@", error.description);
            return;
        }
        NSURL *shortenURL =  shortURL;
        FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
        content.contentURL = shortenURL;
        [FBSDKShareAPI shareWithContent:content delegate:nil];
        
    }];

//
//    dispatch_async(dispatch_get_main_queue(), ^{
//
//    });
//
}
 

/**
 Check Facebook login from anyviewController.
 
 @param VCReferenceObject viewController reference object.
 */
+(void)checkFbLoginforViewController :(UIViewController *)VCReferenceObject
{
    if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
        
    }
    else{
        
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logInWithPublishPermissions:@[@"publish_actions"] fromViewController:VCReferenceObject
                                   handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                       if (error) {
                                           NSLog(@"Process error");
                                       } else if (result.isCancelled)
                                       {
                                           NSLog(@"Cancelled");
                                           [[NSNotificationCenter defaultCenter] postNotificationName:@"facebookCancel" object:nil];
                                       }
                                       else
                                       {
                                           NSLog(@"Logged in");
                                           
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                           });
                                       }
                                   }];
        
    }
    
}


/**
 This method is to convert media links to directory path for Instagram sharing.
 
 @param param postDetails.
 @return path.
 */
+(NSString *)instagramSharing:(NSString *)param
{
    NSString *mediaLink =[Helper getWebLinkForFeed:param];
    NSString *savePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/sharing.igo"];
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:savePath])
    {
        [fm removeItemAtPath:savePath error:nil];
    }
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:mediaLink]]];
    NSData *imageD = UIImageJPEGRepresentation(image, 0.5);
    
    [imageD writeToFile:savePath atomically:YES];
    return savePath;
}


#pragma mark -
#pragma mark - Show Unfollow Alert

/**
 This method is to show alert popup with Ok Action.
 
 @param title alert title.
 @param message alert message.
 @param viewControllerRefrence viewController refrence to present popup.
 */
+(void)showAlertWithTitle:(NSString*)title Message:(NSString*)message viewController :(UIViewController *)viewControllerRefrence
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(alertOk, alertOk) style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okAction ];
    [viewControllerRefrence presentViewController:alertController animated:YES completion:nil];
}

/**
 Email Validation method.
 
 @param emailToValidate email string to validate.
 @return Yes if validation is passed, else NO.
 */
+(BOOL)emailValidationCheck:(NSString *)emailToValidate
{
    NSString *regexForEmailAddress = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regexForEmailAddress];
    return [emailValidation evaluateWithObject:emailToValidate];
}

/**
 Show Background message for No Internet.
 
 @param show Bool Yes to show and NO to hide.
 @param view UIView refrence for frame size.
 @return noInternet customized view.
 */
+(UIView *)showMessageForNoInternet :(BOOL)show forView : (UIView *)view
    {
        UIView *noInternetView = [[UIView alloc] initWithFrame:CGRectMake(0, -64,view.frame.size.width,view.frame.size.height)];
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0,200,100)];
        label.text = @"The Internet connection appears to be offline." ;
        label.textAlignment = NSTextAlignmentCenter ;
        label.numberOfLines = 3 ;
        [label setCenter:noInternetView.center];
        label.hidden = !show ;
        [noInternetView addSubview:label];
        return noInternetView;
    }




/**
 Present Activity Controller with existing apps (Twitter, Instagram etc) for sharing.
 
 @param controller UIActivityController object.
 @param refrenceVC viewController refrence object.
 */
+ (void)presentActivityController:(UIActivityViewController *)controller forViewController:(UIViewController *)refrenceVC{
    
    // for iPad: make the presentation a Popover
    controller.modalPresentationStyle = UIModalPresentationPopover;
    [refrenceVC presentViewController:controller animated:YES completion:nil];
    
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    popController.barButtonItem = refrenceVC.navigationItem.leftBarButtonItem;

    
    controller.excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                         UIActivityTypePrint,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypePostToTencentWeibo,
                                         ];

    
    // access the completion handler
    controller.completionWithItemsHandler = ^(NSString *activityType,
                                              BOOL completed,
                                              NSArray *returnedItems,
                                              NSError *error){
        // react to the completion
        if (completed) {
            // user shared an item
            NSLog(@"We used activity type%@", activityType);
        } else {
            // user cancelled
            NSLog(@"We didn't want to share anything after all.");
        }
        
        if (error) {
            NSLog(@"An Error occured: %@, %@", error.localizedDescription, error.localizedFailureReason);
        }
    };
}





#pragma mark -
#pragma mark - Show Unfollow Alert

/**
 This method show alert for asking permission to unfollow the user.

 @param profieImage     profileImage object of UIImage.
 @param profileName     profileName Of user in string Format.
 @param vcReference     reference of viewController.
 @param completionBlock Bolck will call method only in case of unfollowaction.
 */
+ (void)showUnFollowAlert:(UIImage *)profieImage
                      and:(NSString *)profileName 
           viewControllerReference:(UIViewController *)vcReference
           onComplition:(void (^)(BOOL isUnfollow))completionBlock
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    CGFloat margin = 8.0F;
    UIView *customView;
    customView = [[UIView alloc] initWithFrame:CGRectMake(margin, margin, alertController.view.bounds.size.width - margin * 4.0F, 80)];
    
    UIImageView *UserImageView =[[UIImageView alloc] init];
    UserImageView.image = profieImage;
    UserImageView.frame = CGRectMake(customView.frame.size.width/2-20,10,40,40);
    [vcReference.view layoutIfNeeded];
    UserImageView.layer.cornerRadius = UserImageView.frame.size.height/2;
    UserImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
    UserImageView.layer.borderWidth = 2.0;
    UserImageView.layer.masksToBounds = YES;
    //    customView.backgroundColor = [UIColor colorWithRed:0.9753 green:0.9753 blue:0.9753 alpha:1.0];
    customView.backgroundColor =[UIColor clearColor];
    //creating user name label
    UILabel *UserNamelabel = [[UILabel alloc] init];
    NSString *BoldText = [profileName stringByAppendingString:@"?"];
    NSString *text = [NSString stringWithFormat:@"Unfollow  %@",
                      BoldText];
    
    // If attributed text is supported (iOS6+)
    if ([UserNamelabel respondsToSelector:@selector(setAttributedText:)]) {
        
        // Define general attributes for the entire text
        NSDictionary *attribs = @{
                                  NSForegroundColorAttributeName:UserNamelabel.textColor,
                                  NSFontAttributeName: UserNamelabel.font
                                  };
        
        NSMutableAttributedString *attributedText =
        [[NSMutableAttributedString alloc] initWithString:text
                                               attributes:attribs];
        
        // black and bold text attributes
        UIColor *blackColor = [UIColor colorWithRed:0.1176 green:0.1176 blue:0.1176 alpha:1.0];
        UIFont *boldFont = [UIFont fontWithName:RobotoThin size:14];
        NSRange BoldTextRange = [text rangeOfString:BoldText];
        [attributedText setAttributes:@{NSForegroundColorAttributeName:blackColor,
                                        NSFontAttributeName:boldFont}
                                range:BoldTextRange];
        UserNamelabel.attributedText = attributedText;
    }
    // If attributed text is NOT supported (iOS5-)
    else {
        UserNamelabel.text = text;
    }
    
    [UserNamelabel setFont:[UIFont systemFontOfSize:14]];
    UserNamelabel.frame=CGRectMake(-20,60, vcReference.view.frame.size.width,15);
    UserNamelabel.textAlignment = NSTextAlignmentCenter;
    
    [customView addSubview:UserNamelabel];
    [customView addSubview:UserImageView];
    [alertController.view addSubview:customView];
    
    UIAlertAction *somethingAction = [UIAlertAction actionWithTitle:@"Unfollow" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        completionBlock(YES);
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {}];
    [alertController addAction:somethingAction];
    [alertController addAction:cancelAction];
    [vcReference presentViewController:alertController animated:YES completion:nil];

}


#pragma mark -
#pragma mark - Resize Image  -


/**
 Resize image downloaded from Google/Facebook login.
 
 @param image image object for resize.
 @return resized image.
 */
+ (UIImage *)resizeImage:(UIImage *)image
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 500.0;
    float maxWidth  = 500.0;
    float imgRatio  = actualWidth/actualHeight;
    float maxRatio  = maxWidth/maxHeight;
    float compressionQuality = 0.6;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
    
}

#pragma mark -
#pragma mark - Return CurrencySym -


/**
 This method is invoked to get the currency symbol on the basis of currency.
 
 @param curr currency.
 @return currency symbol.
 */
+(NSString*)returnCurrency:(NSString *)curr
{
    if(curr.length == 0)
    {
        return @"";
    }
    NSLocale *local = [[NSLocale alloc] initWithLocaleIdentifier:curr];
    NSString *currencySym = [NSString stringWithFormat:@"%@",[local displayNameForKey:NSLocaleCurrencySymbol value:curr]];
    return currencySym;
}

/**
 This method returns size of item block for RFQuilt Layout collection.
 
 @param product ProductDetails refrence object.
 @param view UIView refrence from viewController.
 @return CGSize width and Height.
 */
+(CGSize)blockSizeOfProduct :(ProductDetails *)product view:(UIView *)view forHome :(BOOL)isHome
{
    NSInteger width,width2;
    NSInteger height,height2;
    if(view.frame.size.width == 375){
        width = 5;
        height2 = product.imageHeight/30;
        width2 =  product.imageWidth /37;
        height = (width * height2)/width2;
    }
    else{
        width = 2;
        height2 = product.imageHeight/30;
        if(view.frame.size.width == 414)
        {
            width2 =  product.imageWidth/102;
        }
        else
        {
            width2 = product.imageWidth/79;
        }
        
        height = (width * height2)/width2;
    }
    
    
    if(height < 4)
    {
        if(isHome){
            height = height + 3;
        }
        else
        {
            height = height + 1;
        }
        
    }
    else
    {
        if(isHome)
        {
            height = height +2 ;
        }
    }

    return CGSizeMake(width, height);
}


#pragma mark -
#pragma mark - TextField Limit  -


/**
 This method will check limit for Price textfield after decimal.
 
 @param textField price Textfield
 @param string newString
 @param range range
 @return Bool Yes if valid char else NO.
 */
+(BOOL )textFieldLimitAfterDot :(UITextField *)textField  newString:(NSString *)string forRange :(NSRange )range
{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERSFORPRICE] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs]componentsJoinedByString:@""];
    if ([string isEqualToString:filtered]) {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        //checking number of characters after dot.
        NSRange range = [newString rangeOfString:@"."];
        if ([newString containsString:@"."] && range.location == (newString.length - 4))
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    return NO;
}

+(NSString *)currentLanguage
{
    NSString *currentLangauge = flStrForObj([LanguageManager currentLanguageCode]);
    
    if(currentLangauge.length == 0)
    {
        currentLangauge = @"en";
    }
    return currentLangauge;
}

@end
