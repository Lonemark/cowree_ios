
//
//  Created by Rahul Sharma on 7/28/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#ifndef FontDetailsClass_h
#define FontDetailsClass_h

#define RobotoRegular                                 @"Roboto-Regular"
#define RobotoMedium                                @"Roboto-Medium"
#define RobotoThin                                       @"Roboto-Thin"
#define RobotoLight                                      @"Roboto-Light"
#define RobotoBold                                       @"Roboto-Bold"
#define DidotBold                                       @"Didot-Bold"


#endif

